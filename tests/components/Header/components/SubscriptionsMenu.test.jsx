import React from 'react';
import { customRender, fireEvent } from '../../../custom_renderer';
import '@testing-library/jest-dom/extend-expect';
import SubscriptionsMenu from '../../../../src/componentsNew/Header/components/SubscriptionsMenu';

describe('SubscriptionsMenu component', () => {
  const initialState = {
    subscriptions: {
      threads: {},
      count: 0,
    },
  };

  it('displays the menu when the icon is clicked', () => {
    const { getByTitle, queryByText } = customRender(<SubscriptionsMenu />, { initialState });
    fireEvent.click(getByTitle('Subscriptions'));
    expect(queryByText('Subscriptions')).not.toBeNull();
  });

  it('displays an empty state', () => {
    const { getByTitle, queryByText } = customRender(<SubscriptionsMenu />, { initialState });
    fireEvent.click(getByTitle('Subscriptions'));
    expect(queryByText('No new posts')).not.toBeNull();
  });

  it('displays threads with unread posts', () => {
    const state = {
      subscriptions: {
        threads: {
          0: {
            iconId: 1,
            page: 1,
            title: 'Thread',
            count: 3,
          },
          2: {
            iconId: 4,
            page: 2,
            title: 'Second Thread',
            count: 7,
          },
        },
      },
    };
    const { getByTitle, queryByText, queryByTitle } = customRender(<SubscriptionsMenu />, {
      initialState: state,
    });
    fireEvent.click(getByTitle('Subscriptions'));
    expect(queryByText('Thread')).not.toBeNull();
    expect(queryByText('3')).not.toBeNull();
    expect(queryByText('Second Thread')).not.toBeNull();
    expect(queryByText('7')).not.toBeNull();
    expect(queryByTitle('Subscriptions').querySelector('.link-notification')).toHaveTextContent(
      '2'
    );
  });
});
