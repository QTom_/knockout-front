import {
  checkPresence,
  checkBetweenLengths,
  checkLenghGreaterThan,
  checkIsImage,
  checkUserGroup,
  ifPresent,
  validate,
} from '../../src/utils/forms';

import * as UserComponent from '../../src/componentsNew/UserGroupRestricted';

describe('Form utils', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('checkPresence', () => {
    it('returns undefined if the value is present', () => {
      expect(checkPresence('Hello :)')).toBeUndefined();
    });

    it('returns an error if the value is undefined', () => {
      expect(checkPresence()).toEqual('Must have a value');
    });

    it('returns an error if the value is an empty string', () => {
      expect(checkPresence('')).toEqual('Must have a value');
    });
  });

  describe('checkBetweenLengths', () => {
    it('returns undefined if the value is undefined', () => {
      expect(checkBetweenLengths(undefined, 1, 1)).toBeUndefined();
    });

    it('returns undefined if the values length is between the min and max', () => {
      expect(checkBetweenLengths('Hello!', 2, 7)).toBeUndefined();
    });

    it('returns an error if the values length is smaller than the min', () => {
      expect(checkBetweenLengths('H', 2, 7)).toEqual('Must be longer than 2 characters');
    });

    it('returns an error if the values length is smaller than the min', () => {
      expect(checkBetweenLengths('Hello, world!', 2, 7)).toEqual(
        'Must be shorter than 7 characters'
      );
    });
  });

  describe('checkLenghGreaterThan', () => {
    it('returns undefined if the values length is longer than the provided length', () => {
      expect(checkLenghGreaterThan('Hello, world!', 7)).toBeUndefined();
    });

    it('returns an error if the values length is to short', () => {
      expect(checkLenghGreaterThan('Hello!', 7)).toEqual('Must be longer than 7 characters');
    });
  });

  describe('checkIsImage', () => {
    it('returns undefined if the value is an image', () => {
      expect(checkIsImage('https://bbc.co.uk/example.png')).toBeUndefined();
    });

    it('returns an error if the value is not an image', () => {
      expect(checkIsImage('Hello!')).toEqual('Must be an image');
    });
  });

  describe('checkUserGroup', () => {
    it('returns undefined if the user is part of the required usergroup', () => {
      jest.spyOn(UserComponent, 'usergroupCheck').mockImplementation(() => true);
      expect(checkUserGroup([])).toBeUndefined();
    });

    it('returns an error if the value is not an image', () => {
      jest.spyOn(UserComponent, 'usergroupCheck').mockImplementation(() => false);
      expect(checkUserGroup([])).toEqual('USER GROUP INCORRECT');
    });
  });

  describe('ifPresent', () => {
    it('calls and returns the provided function if the value is present', () => {
      const functionMock = jest.fn();
      expect(ifPresent('Hello :)', functionMock)).toBeUndefined();
      expect(functionMock).toBeCalled();
    });

    it('returns undefined if the value is not present', () => {
      const functionMock = jest.fn();
      ifPresent('', functionMock);
      expect(functionMock).not.toBeCalled();
    });
  });

  describe('validate', () => {
    const validators = {
      test: [jest.fn(), jest.fn()],
      other: [jest.fn().mockImplementation(() => 'error'), jest.fn()],
    };

    const values = {
      test: 'Hello :)',
      other: 'world!',
    };

    it('iterates over the validators provided, and returns any errors found', () => {
      validate(values, validators);
      expect(validators.test[0]).toBeCalled();
      expect(validators.test[1]).toBeCalled();
    });

    it('stops calling validators for a field after finding one error', () => {
      expect(validate(values, validators)).toEqual({
        other: 'error',
      });
      expect(validators.other[0]).toBeCalled();
      expect(validators.other[1]).not.toBeCalled();
    });
  });
});
