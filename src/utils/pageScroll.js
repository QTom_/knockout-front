export const HEADER_HEIGHT = 50;
export const MOTD_HEIGHT = 40;
export const MOTD_HEIGHT_MOBILE = 80;

export const scrollToBottom = (timeout) =>
  setTimeout(() => {
    window.scrollTo(0, document.body.scrollHeight);
  }, timeout || 10);

export const scrollToTop = (timeout) =>
  setTimeout(() => {
    window.scrollTo(0, 0);
  }, timeout);
export const scrollIntoView = (querySelector) => {
  if (!querySelector) return;
  const element = document.querySelector(querySelector);
  if (!element) return;
  window.scrollTo({ top: element.offsetTop - HEADER_HEIGHT, behavior: 'auto' });
};
