import dayjs from 'dayjs';

const PERMABAN_PREFIXES = ['a length of time', 'a duration', 'a period of time', 'a span of time'];

const PERMABAN_SUFFIXES = [
  'that surpasses the heat death of the universe',
  'incomprehensible by human minds',
  'longer than you can conceive',
];

const randomPermabanMessage = () => {
  const prefix = PERMABAN_PREFIXES[Math.floor(Math.random() * PERMABAN_PREFIXES.length)];
  const suffix = PERMABAN_SUFFIXES[Math.floor(Math.random() * PERMABAN_SUFFIXES.length)];

  return `${prefix} ${suffix}`;
};

const humanizeDuration = (start, end) => {
  const startTime = dayjs(start);
  const endTime = dayjs(end);

  const hoursLength = endTime.diff(startTime) / 3600000;
  let diffType;

  if (hoursLength < 24) {
    diffType = 'hours';
  } else if (hoursLength >= 24 && hoursLength < 168) {
    diffType = 'days';
  } else if (hoursLength >= 168 && hoursLength < 744) {
    diffType = 'weeks';
  } else if (hoursLength >= 744 && hoursLength < 8760) {
    diffType = 'months';
  } else if (hoursLength < 43800) {
    diffType = 'years';
  } else {
    return randomPermabanMessage();
  }

  const hourDifference = endTime.diff(startTime, diffType);
  if (hourDifference === 1) {
    diffType = diffType.substring(0, diffType.length - 1);
  }

  return `${hourDifference} ${diffType}`;
};

export default humanizeDuration;
