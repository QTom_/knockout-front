/* eslint-disable no-console */
/* eslint-disable camelcase */
import axios from 'axios';

import config from '../../config';

import { authPut, authGet, authDelete } from './common';
import { pushNotification, pushSmartNotification } from '../utils/notification';
import { saveBannedMessageToStorage, clearBannedMessageFromStorage } from '../utils/bannedStorage';

export const updateUser = (data) => authPut({ url: '/user', data });

export const loadUserFromStorage = () => {
  const user = JSON.parse(localStorage.getItem('currentUser'));

  if (user) {
    const { username, avatar_url, background_url, usergroup, id, createdAt } = user;

    return { username, avatar_url, background_url, usergroup, id, createdAt };
  }
  return null;
};

export const saveUserToStorage = (data) => {
  const { username, avatar_url, background_url, usergroup, id, createdAt } = data.user;
  const user = JSON.stringify({ username, avatar_url, background_url, usergroup, id, createdAt });

  localStorage.setItem('currentUser', user);
};

export const googleLogin = async (googleResponse) => {
  const res = await axios.get(`${config.apiHost}/auth/google`, {
    headers: {
      gtoken: googleResponse.tokenId,
    },
  });

  const { data } = res;

  return data;
};

export const removeUserFromStorage = () => {
  localStorage.removeItem('currentUser');
};

export const getUser = async (userId) => {
  // if logged in, get the user with auth
  const user = localStorage.getItem('currentUser');
  if (user) {
    const res = await authGet({ url: `/user/${userId}` });
    const { data } = res;
    return data;
  }

  const res = await axios.get(`${config.apiHost}/user/${userId}`);
  const { data } = res;
  return data;
};

export const getUserBans = async (userId) => {
  const res = await axios.get(`${config.apiHost}/user/${userId}/bans`);
  const { data } = res;
  return data;
};

export const getUserTopRatings = async (userId) => {
  try {
    const res = await axios.get(`${config.apiHost}/user/${userId}/topRatings`);
    const { data } = res;
    return data;
  } catch (error) {
    return [];
  }
};

export const getUserProfile = async (userId) => {
  try {
    const res = await axios.get(`${config.apiHost}/user/profile/${userId}/`);
    const { data } = res;
    return data;
  } catch (error) {
    pushSmartNotification(error);
    return [];
  }
};

export const updateUserProfile = async (data) => {
  try {
    const formData = new FormData();
    formData.append('backgroundImage', data.backgroundImage);

    Object.keys(data).forEach((key) => {
      if (key === 'backgroundImage') {
        return;
      }
      formData.append(key, data[key]);
    });

    const res = await authPut({
      url: `/user/profile`,
      data: formData,
      headers: {
        'Content-Type': 'multipart/form-data',
      },
    });

    return pushSmartNotification(res.data);
  } catch (error) {
    pushSmartNotification({ error: 'Could not update profile. Is your image under 2MB?' });
    return {};
  }
};

export const getUserPosts = async (userId, page = 1) => {
  const res = await axios.get(`${config.apiHost}/user/${userId}/posts/${page}`);
  const { data } = res;
  return data;
};

export const getUserThreads = async (userId, page = 1) => {
  const res = await axios.get(`${config.apiHost}/user/${userId}/threads/${page}`);
  const { data } = res;
  return data;
};

export function setupUserFlags(user) {
  const newUser = { ...user };
  if (user.id === null) {
    newUser.loggedIn = false;
  } else {
    newUser.loggedIn = true;
    if (user.username === null) {
      newUser.requiresSetup = true;
    } else {
      newUser.requiresSetup = false;
    }
  }

  newUser.isModerator = user.usergroup >= 3;

  return newUser;
}

export const syncData = async () => {
  try {
    const results = await authGet({
      url: '/user/syncData',
      data: {},
      headers: { 'Cache-Control': 'private, no-store, max-age=0' },
    });

    if (!results || (results && results.data.error)) {
      throw new Error('Could not fetch your user data.');
    }

    const userData = results.data;

    // handle bans
    // logging them out is done in CheckLoginStatus
    if (userData.isBanned) {
      saveBannedMessageToStorage(userData.banInfo);

      pushNotification({
        message:
          'Uh oh! Looks like you are banned! 😂 Banned users do not have the same privileges as normal users. 😔',
        type: 'error',
      });
    } else {
      clearBannedMessageFromStorage();
    }

    if (!userData) {
      throw new Error('Could not fetch data.');
    }

    // update local user data
    saveUserToStorage({
      user: {
        id: userData.id,
        username: userData.username,
        avatar_url: userData.avatarUrl,
        background_url: userData.backgroundUrl,
        usergroup: userData.usergroup,
        createdAt: userData.createdAt,
      },
    });

    return results.data;
  } catch (err) {
    console.error(err);

    return { error: true };
  }
};

export const updateProfileRatingsDisplay = async (ratingsHiddenFromProfile) => {
  // HACK: Remove the following line once the backend supports booleans
  ratingsHiddenFromProfile = ratingsHiddenFromProfile ? 'true' : 'false';

  try {
    const res = await authPut({
      url: `/user/updateProfileRatingsDisplay`,
      data: {
        hideProfileRatings: ratingsHiddenFromProfile,
      },
    });

    return pushSmartNotification(res.data);
  } catch (error) {
    pushSmartNotification(error);
    return {};
  }
};

export const getProfileRatingsDisplay = async () => {
  try {
    const res = await authGet({
      url: `/user/profileRatingsDisplay`,
      data: {},
    });
    return res.data.ratingsHiddenForUser;
  } catch (error) {
    pushSmartNotification(error);
    return false;
  }
};

export const deleteOwnAccount = async () => {
  try {
    const { id } = loadUserFromStorage();

    const results = await authDelete({ url: `/user/${id}`, data: {} });

    return results.data;
  } catch (err) {
    console.error(err);

    return { error: true };
  }
};
