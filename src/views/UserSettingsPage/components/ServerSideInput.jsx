/* eslint-disable jsx-a11y/label-has-for */
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { StyledOptionsStorageInput } from './OptionsStorageInput';
import { FieldLabel, FieldLabelSmall } from '../../../componentsNew/FormControls';
import FormSwitch from '../../../componentsNew/FormControls/components/FormSwitch';

const ServerSideInput = (props) => {
  const [value, setValue] = useState(false);

  useEffect(() => {
    const fetchValue = async () => {
      const serverSideValue = await props.getValue();
      setValue(serverSideValue);
    };
    fetchValue();
  }, []);

  const updateValue = (newValue) => {
    const setValueAsync = async () => {
      await props.setValue(newValue);
      setValue(newValue);
    };
    setValueAsync();
  };
  const { desc, label } = props;

  return (
    <StyledOptionsStorageInput>
      <div className="options-info">
        <FieldLabel>{label}</FieldLabel>
        <FieldLabelSmall>{desc}</FieldLabelSmall>
      </div>
      <FormSwitch
        checked={value}
        setChecked={() => {
          updateValue(!value);
        }}
      />
    </StyledOptionsStorageInput>
  );
};

ServerSideInput.propTypes = {
  label: PropTypes.string.isRequired,
  desc: PropTypes.string.isRequired,
  setValue: PropTypes.func,
  getValue: PropTypes.func,
};

ServerSideInput.defaultProps = {
  setValue: null,
  getValue: null,
};

export default ServerSideInput;
