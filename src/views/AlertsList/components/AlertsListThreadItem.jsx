import React from 'react';
import PropTypes from 'prop-types';
import ThreadItem from '../../../componentsNew/ThreadItem';

const AlertsListThreadItem = ({
  id,
  createdAt,
  deletedAt,
  iconId,
  user,
  lastPost,
  locked,
  postCount,
  unreadPostCount,
  title,
  firstUnreadId,
  backgroundUrl,
  backgroundType,
  markUnreadAction,
}) => (
  <ThreadItem
    id={id}
    createdAt={createdAt}
    deleted={deletedAt && deletedAt.length > 0}
    iconId={iconId}
    user={user}
    lastPost={lastPost}
    locked={locked}
    postCount={postCount}
    unreadPostCount={unreadPostCount}
    title={title}
    firstUnreadId={firstUnreadId}
    backgroundUrl={backgroundUrl}
    backgroundType={backgroundType}
    markUnreadAction={markUnreadAction}
    markUnreadText="Unsubscribe"
    showTopRating={false}
  />
);

AlertsListThreadItem.propTypes = {
  id: PropTypes.number.isRequired,
  createdAt: PropTypes.string.isRequired,
  deletedAt: PropTypes.string,
  iconId: PropTypes.number.isRequired,
  user: PropTypes.shape({
    avatarUrl: PropTypes.string,
    username: PropTypes.string.isRequired,
    usergroup: PropTypes.number.isRequired,
  }).isRequired,
  lastPost: PropTypes.shape({
    id: PropTypes.number.isRequired,
    created_at: PropTypes.string.isRequired,
    user: PropTypes.shape({
      avatar_url: PropTypes.string,
      username: PropTypes.string.isRequired,
      usergroup: PropTypes.number.isRequired,
    }).isRequired,
  }).isRequired,
  locked: PropTypes.bool,
  postCount: PropTypes.number.isRequired,
  unreadPostCount: PropTypes.number,
  title: PropTypes.string.isRequired,
  firstUnreadId: PropTypes.number,
  backgroundUrl: PropTypes.string,
  backgroundType: PropTypes.string,
  markUnreadAction: PropTypes.func.isRequired,
};

AlertsListThreadItem.defaultProps = {
  deletedAt: undefined,
  locked: false,
  unreadPostCount: 0,
  firstUnreadId: undefined,
  backgroundUrl: '',
  backgroundType: '',
};

export default AlertsListThreadItem;
