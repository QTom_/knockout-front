/* eslint-disable react/forbid-prop-types */
import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import ratingList from '../../../components/Rating/ratingList.json';

const UserProfileRatings = ({ ratings }) => {
  if (ratings.length === 0) {
    return <p>You have no ratings... how?</p>;
  }

  return (
    <StyledUserProfileRatings>
      <tbody>
        {ratings
          .sort((a, b) => (a.count > b.count ? -1 : 1))
          .map((rating, i) => (
            <tr key={rating}>
              <td>
                <img
                  style={{
                    maxWidth: i === 0 ? 'unset' : '25px',
                    maxHeight: i === 0 ? 'unset' : '25px',
                    verticalAlign: 'middle',
                    paddingRight: i === 0 ? '10px' : '5px',
                  }}
                  src={ratingList[rating.name].url}
                  alt={ratingList[rating.name].name}
                />
              </td>
              <td>
                <div
                  className="ratings-info"
                  style={{
                    fontSize: i === 0 ? '30px' : 'unset',
                  }}
                >
                  <div>
                    <span>{ratingList[rating.name].name}</span>
                    &nbsp;
                    <span>x</span>
                    <b>{rating.count}</b>
                  </div>
                </div>
              </td>
            </tr>
          ))}
      </tbody>
    </StyledUserProfileRatings>
  );
};

const StyledUserProfileRatings = styled.table`
  .ratings-info {
    margin-top: 20px;
  }
`;

UserProfileRatings.propTypes = {
  ratings: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      url: PropTypes.string.isRequired,
    })
  ).isRequired,
};

export default UserProfileRatings;
