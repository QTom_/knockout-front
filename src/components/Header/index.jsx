/* eslint-disable react/forbid-prop-types */
/* eslint-disable react/jsx-one-expression-per-line */
import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { updateMentions } from '../../state/mentions';

import { loadBannedMessageFromStorage } from '../../utils/bannedStorage';

import { isLoggedIn } from '../LoggedInOnly';
import CheckLoginStatus from '../FunctionalComponents/CheckLoginStatus';
import HeaderComponent from './Header';
import { updateBackgroundRequest } from '../../state/background';
import { loadStickyHeaderFromStorageBoolean } from '../../services/theme';

class Header extends React.Component {
  lastScrollY = 0;

  ticking = false;

  constructor(props) {
    super(props);
    this.state = {
      onTopOfPage: true,
    };
  }

  async componentDidMount() {
    window.addEventListener('scroll', this.handleScroll, true);

    if (!isLoggedIn()) {
      const bannedInformation = loadBannedMessageFromStorage();

      if (!bannedInformation) {
        return;
      }

      this.setState({ bannedInformation });
    }
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  handleScroll = () => {
    this.lastScrollY = window.scrollY;

    if (!this.ticking) {
      window.requestAnimationFrame(() => {
        if (this.state.onTopOfPage && this.lastScrollY > 5) {
          this.setState({
            onTopOfPage: false,
          });
        }
        if (!this.state.onTopOfPage && this.lastScrollY <= 5) {
          this.setState({ onTopOfPage: true });
        }

        this.ticking = false;
      });

      this.ticking = true;
    }
  };

  updateHeader = ({ mentions, subscriptions, reports }) => {
    if ((subscriptions && subscriptions[0]) || reports) {
      const unreadSubscriptions = subscriptions.reduce(
        (total, thread) => total + thread.unreadPosts,
        0
      );
      this.setState({
        unreadSubscriptions: unreadSubscriptions > 0 ? unreadSubscriptions : null,
        openReports: reports > 0 ? reports : null,
      });
    }

    if (mentions && mentions[0]) {
      this.props.updateMentionsAction(mentions);
    }
  };

  removeMention = (mentionId) => {
    if (!this.state.mentions) {
      return;
    }

    const mentionIndex = this.state.mentions.findIndex((el) => el.mentionId === mentionId);

    if (mentionIndex >= 0) {
      /* eslint-disable react/no-access-state-in-setstate */
      const newMentionArray = [...this.state.mentions];
      newMentionArray.splice(mentionIndex, 1);

      this.setState({
        mentions: newMentionArray,
      });
    }
  };

  clearSubscriptions = () => this.setState({ unreadSubscriptions: undefined });

  clearReports = () => this.setState({ openReports: undefined });

  render() {
    const { bannedInformation, unreadSubscriptions, openReports, onTopOfPage } = this.state;
    const { mentionState } = this.props;

    return (
      <>
        <HeaderComponent
          onTopOfPage={onTopOfPage}
          bannedInformation={bannedInformation}
          updateHeader={this.updateHeader}
          unreadSubscriptions={unreadSubscriptions}
          openReports={openReports}
          mentionState={mentionState}
          getMentions={this.getMentions}
          clearSubscriptions={this.clearSubscriptions}
          clearReports={this.clearReports}
          stickyHeader={loadStickyHeaderFromStorageBoolean()}
        />
        <CheckLoginStatus updateHeader={this.updateHeader} />
      </>
    );
  }
}

const mapStateToProps = ({ user, mentions }) => ({ user, mentionState: mentions.mentions });
const mapDispatchToProps = (dispatch) => ({
  updateMentionsAction: (val) => dispatch(updateMentions(val)),
  updateBackground: (val) => dispatch(updateBackgroundRequest(val)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Header);

Header.propTypes = {
  user: PropTypes.shape({
    avatar_url: PropTypes.string,
    username: PropTypes.string,
    loggedIn: PropTypes.bool,
    requiresSetup: PropTypes.bool,
    usergroup: PropTypes.number,
    createdAt: PropTypes.string,
  }),
  updateMentionsAction: PropTypes.func.isRequired,
  mentionState: PropTypes.array.isRequired,
};

Header.defaultProps = {
  user: {
    avatar_url: undefined,
    username: undefined,
    loggedIn: false,
    requiresSetup: true,
    usergroup: undefined,
    createdAt: undefined,
  },
};
