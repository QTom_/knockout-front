import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import userRoles from './userRoles';

/**
 * @param {{ user: { usergroup: number, isBanned: boolean }, children: React.DOMElement[] }} props
 */
const UserRoleWrapper = (props) => {
  const userGroupId = (props.user && props.user.usergroup) || 1;
  const userRole = userRoles[userGroupId] ? userRoles[userGroupId] : userRoles[1];
  const isBanned = (props.user && props.user.isBanned) || false;
  const title = isBanned ? `Banned ${userRole.name}` : userRole.name;

  return (
    <StyledUserRoleWrapper userRole={userRole} title={title} isBanned={isBanned}>
      {props.children}
    </StyledUserRoleWrapper>
  );
};

export const StyledUserRoleWrapper = styled.span`
  color: ${(props) => props.userRole.color};

  ${(props) => props.isBanned && 'color: #f44336;'}

  ${(props) => props.userRole.extraStyle};
`;

UserRoleWrapper.propTypes = {
  user: PropTypes.shape({
    usergroup: PropTypes.number.isRequired,
    isBanned: PropTypes.bool,
  }),
  children: PropTypes.node.isRequired,
};
UserRoleWrapper.defaultProps = {
  user: { isBanned: false },
};

export default UserRoleWrapper;
