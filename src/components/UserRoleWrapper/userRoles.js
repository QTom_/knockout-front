/* stylelint-disable property-no-vendor-prefix */
import { css } from 'styled-components';
import { ThemeGoldMemberColor, ThemeGoldMemberGlow } from '../../Theme';

export default {
  0: {
    name: 'Banned User',
    color: '#f00',
    extraStyle: null,
  },
  1: {
    name: 'Member',
    color: '#3facff',
    extraStyle: null,
  },
  2: {
    name: 'Gold Member',
    color: '#fcbe20',
    extraStyle: css`
      color: transparent;
      filter: ${ThemeGoldMemberGlow};
      background: ${ThemeGoldMemberColor};
      background-clip: text;
      -webkit-background-clip: text;
    `,
  },
  3: {
    name: 'Moderator',
    color: '#41ff74',
    extraStyle: css`
      text-shadow: 1px 1px rgba(0, 0, 0, 0.2);
    `,
  },
  4: {
    name: 'Admin',
    color: '#fcbe20',
    extraStyle: css`
      color: transparent;
      filter: ${ThemeGoldMemberGlow};
      background: ${ThemeGoldMemberColor};
      background-clip: text;
      -webkit-background-clip: text;
    `,
  },
  5: {
    name: 'Staff',
    color: '#fcbe20',
    extraStyle: css`
      color: transparent;
      filter: ${ThemeGoldMemberGlow};
      background: ${ThemeGoldMemberColor};
      background-clip: text;
      -webkit-background-clip: text;
    `,
  },
  6: {
    name: 'Moderator in Training',
    color: '#4ccf6f',
    extraStyle: css`
      text-shadow: 1px 1px rgba(0, 0, 0, 0.2);
    `,
  },
};
