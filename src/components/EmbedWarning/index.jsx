/* eslint-disable react/jsx-one-expression-per-line */
/* eslint-disable react/forbid-prop-types */
import React from 'react';
import PropTypes from 'prop-types';
import StyledEmbedWarning from './style';
import { saveEmbedSettingToStorage } from '../../utils/thirdPartyEmbedStorage';

const EmbedWarning = ({ serviceName, attributes, selected = false }) => (
  <StyledEmbedWarning {...attributes} selected={selected}>
    <img src="https://cdn.knockout.chat/assets/warning.png" alt="Warning icon" />
    <p>
      <b>Watch out!</b>
    </p>
    <div>
      This 3rd party {serviceName} embed might track your data or do something else - it&apos;s out
      of our hands.
    </div>
    <p>
      You can <b>permanently</b> unlock 3rd party embeds <b>at your own risk</b> by clicking the
      button below:
    </p>
    <button onClick={() => saveEmbedSettingToStorage()} type="button">
      Unlock embeds
    </button>
  </StyledEmbedWarning>
);

export default EmbedWarning;

EmbedWarning.propTypes = {
  serviceName: PropTypes.string,
  attributes: PropTypes.object,
  selected: PropTypes.bool.isRequired,
};
EmbedWarning.defaultProps = {
  serviceName: 'service',
  attributes: {},
};
