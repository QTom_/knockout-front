import styled from 'styled-components';
import {
  ThemeHorizontalPadding,
  ThemeVerticalPadding,
  ThemeVerticalHalfPadding,
  ThemeSecondaryBackgroundColor,
  ThemeQuaternaryBackgroundColor,
  ThemePrimaryTextColor
} from '../../Theme';

export const EventList = styled.div`
  padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
`;

export const EventDescription = styled.div`
  color: ${ThemePrimaryTextColor};
  background: ${ThemeSecondaryBackgroundColor};
  padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
  margin-bottom: ${ThemeVerticalPadding};
  border-radius: 5px;
`;

export const StyledEventCardWrapper = styled.div`
  width: 100%;
  display: flex;
  padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
  margin: ${ThemeVerticalHalfPadding} 0;
  background: ${ThemeQuaternaryBackgroundColor};
  border-radius: 5px;
  box-sizing: border-box;
  color: ${ThemePrimaryTextColor};
`;

export const StyledEventDate = styled.div`
  span {
    display: none;
    font-family: monospace;
  }

  &:hover {
    span {
      display: inline-block;
    }
  }

  margin-right: 5px;
`;

export const StyledEventCardBody = styled.div`
  a {
    color: #3facff;
  }
`;
