/* eslint-disable react/jsx-one-expression-per-line */
import React from 'react';
import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';

import ForumIcon from '../ForumIcon';
import { getPopularThreads } from '../../services/popularThreads';
import { getLatestThreads } from '../../services/latestThreads';
import {
  loadLatestThreadModeFromStorage,
  setLatestThreadModeToStorage
} from '../../services/theme';

import {
  PopularThreadWrapper,
  PopularThreadList,
  PopularThreadHeader,
  PopularThreadModeSwitch,
  PopularThreadTitle,
  PopularThreadDescription,
  PopularThreadContent,
  PopularThreadContentScoller,
  PopularThreadContentShadow,
  PopularThreadItem,
  PopularThreadItemIcon,
  PopularThreadItemContent,
  PopularThreadItemTitle,
  PopularThreadItemDescription,
  StyledItemPlaceholder
} from './style';
import Tooltip from '../Tooltip';

dayjs.extend(relativeTime);

class PopularThreads extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      latestThreads: [],
      popularThreads: []
    };

    this.changeMode = this.changeMode.bind(this);
  }

  async componentDidMount() {
    this.setState(
      {
        showLatest: loadLatestThreadModeFromStorage()
      },
      this.getThreads
    );
  }

  async getThreads() {
    const { latestThreads, popularThreads, showLatest } = this.state;

    if (showLatest && latestThreads.length === 0) {
      const threads = await getLatestThreads();

      this.setState({
        latestThreads: threads.list
      });
    }

    if (!showLatest && popularThreads.length === 0) {
      const threads = await getPopularThreads();

      this.setState({
        popularThreads: threads.list
      });
    }

    setLatestThreadModeToStorage(showLatest);
  }

  blockTitle = () => {
    if (this.state.latestThreads.length === 0 && this.state.popularThreads.length === 0) {
      return 'Loading...';
    }

    return this.state.showLatest ? 'Latest Threads' : 'Popular Threads';
  };

  blockSubtitle = () => {
    if (this.state.latestThreads.length === 0 && this.state.popularThreads.length === 0) {
      return 'Timewasters coming right up!';
    }

    return this.state.showLatest ? 'Whats new?' : 'See where all the fun is at';
  };

  changeMode() {
    this.setState(
      prevState => ({
        showLatest: !prevState.showLatest
      }),
      this.getThreads
    );
  }

  renderItems() {
    const { latestThreads, popularThreads, showLatest } = this.state;

    const Placeholder = () => (
      <StyledItemPlaceholder>
        <div className="fake-icon" />
        <PopularThreadItemContent>
          <div className="fake-title" />
          <div className="fake-info" />
        </PopularThreadItemContent>
      </StyledItemPlaceholder>
    );

    const threads = showLatest ? latestThreads : popularThreads;

    if ((latestThreads.length === 0 && popularThreads.length === 0) || threads.length === 0) {
      return Array(16)
        .fill(16)
        .map((o, i) => <Placeholder key={`phitem${i * 3}`} />);
    }

    return threads.map((thread, index) => (
      <Tooltip
        key={thread.id}
        top={index !== 0}
        text={`Created by ${thread.user.username}, last reply ${dayjs(thread.updatedAt).fromNow()}`}
        widthLimited
      >
        <PopularThreadItem to={`/thread/${thread.id}`}>
          <PopularThreadItemIcon>
            <ForumIcon iconId={thread.iconId} />
          </PopularThreadItemIcon>
          <PopularThreadItemContent>
            <PopularThreadItemTitle>{thread.title}</PopularThreadItemTitle>
            {showLatest ? (
              <PopularThreadItemDescription>
                Posted {dayjs(thread.createdAt).fromNow()}
              </PopularThreadItemDescription>
            ) : (
              <PopularThreadItemDescription>
                {thread.recentPostCount} recent replies
              </PopularThreadItemDescription>
            )}
          </PopularThreadItemContent>
        </PopularThreadItem>
      </Tooltip>
    ));
  }

  render() {
    const { showLatest } = this.state;

    return (
      <PopularThreadWrapper>
        <PopularThreadList>
          <PopularThreadHeader>
            <PopularThreadTitle>{this.blockTitle()}</PopularThreadTitle>
            <PopularThreadDescription>{this.blockSubtitle()}</PopularThreadDescription>
            <PopularThreadModeSwitch
              onClick={this.changeMode}
              title={showLatest ? 'Show popular threads' : 'Show latest threads'}
            >
              <i className={showLatest ? 'fas fa-fire' : 'fas fa-clock'} />
            </PopularThreadModeSwitch>
          </PopularThreadHeader>
          <PopularThreadContent>
            <PopularThreadContentScoller>{this.renderItems()}</PopularThreadContentScoller>
            <PopularThreadContentShadow />
          </PopularThreadContent>
        </PopularThreadList>
      </PopularThreadWrapper>
    );
  }
}

export default PopularThreads;
