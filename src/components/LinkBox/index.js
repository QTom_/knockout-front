import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { ThemeHorizontalHalfPadding } from '../../Theme';

const LinkBox = styled(Link)`
  position: relative;
  border-radius: 5px;
  flex-grow: 1;
  margin: 0 ${ThemeHorizontalHalfPadding};
  color: white;
  text-decoration: none;
  display: block;
  overflow: hidden;
  text-align: left;
`;

export default LinkBox;
