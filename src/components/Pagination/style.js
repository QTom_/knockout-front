import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { ThreadPageItem } from '../../pages/SubforumPage/components/style';
import { buttonHover, MobileMediaQuery, TabletMediaQuery } from '../SharedStyles';
import {
  ThemeHorizontalHalfPadding,
  ThemeSmallTextSize,
  ThemeTertiaryBackgroundColor,
  ThemeQuaternaryBackgroundColor,
  ThemePrimaryTextColor,
  ThemeBodyColor,
  ThemeVerticalPadding
} from '../../Theme';

export const PaginationWrapper = styled.div`
  display: flex;
  flex-direction: row;
  margin: 0;
  height: 30px;
  text-align: center;
  background: ${ThemeTertiaryBackgroundColor};
  border-radius: 5px;
  overflow: hidden;

  ${TabletMediaQuery} {
    width: 100%;
    max-width: unset;
  }

  ${props =>
    props.small &&
    `
      min-width: unset;
      flex-grow: 0;
      height: unset;
      width: fit-content !important;
      overflow: hidden;
  `}

  ${props =>
    props.marginBottom &&
    `
      margin-bottom: 8px;
      margin-bottom: ${ThemeVerticalPadding};
  `}
`;

export const PaginationItem = styled(Link)`
  box-sizing: border-box;
  text-align: center;
  padding: 0 ${ThemeHorizontalHalfPadding};
  flex-grow: 1;
  line-height: 30px;
  margin: 0;
  display: inline-block;
  font-size: ${ThemeSmallTextSize};
  color: ${ThemePrimaryTextColor};
  text-decoration: none;
  transition: border-color 0.15s ease-in-out;

  &:nth-child(2n) {
    border-left: 1px solid ${ThemeBodyColor};
  }
  &:nth-child(1n) {
    border-right: 1px solid ${ThemeBodyColor};
  }
  &:last-child(),
  &:first-child() {
    border: none;
  }

  ${buttonHover}

  ${MobileMediaQuery} {
    padding-left: 0;
    padding-right: 0;
  }

  ${ThreadPageItem} & {
    border-width: 1px;
    margin: 0;
  }

  ${props =>
    props.small &&
    `
      line-height: 1.5;

      ${MobileMediaQuery} {
        padding: 2px 7px;
      }
  `}
`;

export const PaginationItemSpacer = styled.span`
  box-sizing: border-box;
  text-align: center;
  padding: 0 ${ThemeHorizontalHalfPadding};
  min-width: 25px;
  line-height: 30px;
  margin: 0;
  display: inline-block;
  font-size: ${ThemeSmallTextSize};
  color: ${ThemePrimaryTextColor};
  text-decoration: none;
  transition: border-color 0.15s ease-in-out;

  ${MobileMediaQuery} {
    padding-left: 0;
    padding-right: 0;
  }

  ${props =>
    props.small &&
    `
      line-height: 1.5;
  `}
`;

export const PaginationItemActive = styled(PaginationItem)`
  font-weight: bold;
  background: ${ThemeQuaternaryBackgroundColor};
  position: relative;

  &:after {
    font-family: 'Font Awesome 5 Free', sans-serif;
    font-weight: 900;
    content: '\f0de';
    position: absolute;
    top: 56%;
    left: 50%;
    transform: translateX(-50%);
    height: 5px;
    opacity: 1;
    color: #ec3737;
  }
`;

export const PaginationItemButton = styled.button`
  box-sizing: border-box;
  text-align: center;
  padding: 0 ${ThemeHorizontalHalfPadding};
  flex-grow: 1;
  line-height: 30px;
  margin: 0;
  display: inline-block;
  font-size: ${ThemeSmallTextSize};
  color: ${ThemePrimaryTextColor};
  text-decoration: none;
  transition: border-color 0.15s ease-in-out;
  border: none;
  background: ${ThemeTertiaryBackgroundColor};

  &:nth-child(2n) {
    border-left: 1px solid ${ThemeBodyColor};
  }
  &:nth-child(1n) {
    border-right: 1px solid ${ThemeBodyColor};
  }
  &:last-child(),
  &:first-child() {
    border: none;
  }

  ${buttonHover}

  ${MobileMediaQuery} {
    padding-left: 0;
    padding-right: 0;
  }

  ${ThreadPageItem} & {
    border-width: 1px;
    margin: 0;
  }

  ${props =>
    props.small &&
    `
      line-height: 1.5;

      ${MobileMediaQuery} {
        padding: 2px 7px;
      }
  `}
`;

export const PaginationItemActiveButton = styled(PaginationItemButton)`
  font-weight: bold;
  background: ${ThemeQuaternaryBackgroundColor};
  position: relative;

  &:after {
    font-family: 'Font Awesome 5 Free', sans-serif;
    font-weight: 900;
    content: '\f0de';
    position: absolute;
    top: 56%;
    left: 50%;
    transform: translateX(-50%);
    height: 5px;
    opacity: 1;
    color: #ec3737;
  }
`;
