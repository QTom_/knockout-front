import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const StyledVocaroo = styled.iframe`
  width: 100%;
  max-width: 350px;
  height: 80px;
`;

export const getVocarooId = (url) => {
  const regex = /(https:\/\/(?:www\.|(?!www))(vocaroo)\.com\/([\d\w]+))/;

  try {
    const result = regex.exec(url);

    if (!result[3]) {
      return null;
    }

    return result[3];
  } catch (error) {
    return null;
  }
};

class VocarooBB extends React.Component {
  state = { visible: false };

  onChange = (isVisible) => isVisible && this.setState({ visible: isVisible });

  render() {
    try {
      const { content, href } = this.props;
      const vocarooUrl = href || `${content}`;
      const vocarooId = getVocarooId(vocarooUrl);

      return (
        <StyledVocaroo
          title={vocarooId}
          type="text/html"
          width="100%"
          height="auto"
          src={`https://vocaroo.com/embed/${vocarooId}`}
          frameBorder="0"
          allowFullScreen="allowfullscreen"
        />
      );
    } catch (error) {
      return '[Bad Vocaroo embed.]';
    }
  }
}
VocarooBB.propTypes = {
  src: PropTypes.string.isRequired,
  selected: PropTypes.bool.isRequired,
  // eslint-disable-next-line react/forbid-prop-types
  attributes: PropTypes.any,
};

VocarooBB.defaultProps = {
  attributes: undefined,
};

export default VocarooBB;
