import React from 'react';
import PropTypes from 'prop-types';
import VisibilitySensor from 'react-visibility-sensor';

export const getStrawPollId = href => {
  const strawPollRegx = /^http[s]?:\/\/[www.]*strawpoll\.me\/(?:#!\/)?([0-9]+)/;
  const poll = strawPollRegx.exec(href);
  if (!poll) return null;
  const pollId = poll[1];
  if (!pollId) return null;
  return pollId;
};
class StrawPollBB extends React.Component {
  state = { visible: false };

  onChange = isVisible => isVisible && this.setState({ visible: isVisible });

  render() {
    const { href, children } = this.props;
    const strawpollUrl = href || children.join('');
    const strawPollId = getStrawPollId(strawpollUrl);

    return this.state.visible ? (
      <iframe
        title="Poll on Straw Poll"
        src={`https://www.strawpoll.me/embed_1/${strawPollId}`}
        style={{ width: '100%', maxWidth: '680px', height: '485px' }}
      />
    ) : (
      <VisibilitySensor
        partialVisibility
        minTopValue={150}
        intervalDelay="250"
        onChange={this.onChange}
      >
        <img alt="Straw Poll placeholder" src="/static/straw_poll_placeholder.webp" />
      </VisibilitySensor>
    );
  }
}
StrawPollBB.propTypes = {
  href: PropTypes.string.isRequired,
};
export default StrawPollBB;
