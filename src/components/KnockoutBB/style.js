/* eslint-disable react/jsx-curly-newline */
import styled from 'styled-components';

import { buttonHover } from '../SharedStyles';

import {
  ThemeMediumTextSize,
  ThemeHorizontalPadding,
  ThemeQuaternaryBackgroundColor,
  ThemePrimaryTextColor,
  ThemeVerticalPadding,
  ThemeBackgroundLighter,
} from '../../Theme';

export const Button = styled.span`
  display: inline-block;
  padding: 5px;
  color: ${ThemePrimaryTextColor};
  text-align: center;
  vertical-align: middle;
  cursor: pointer;
  ${(props) =>
    props.active &&
    `
    i {
      color: #ec3737;
    }
  `}
`;

export const Menu = styled.div`
  & > * {
    display: inline-block;
  }
`;

export const Toolbar = styled(Menu)`
  position: relative;
  flex-grow: 1;
  box-sizing: border-box;
  padding: 5px;

  @media (max-width: 900px) {
    ${Button} {
      padding: 10px;
      width: 36px;
      box-sizing: border-box;
    }
  }
`;

export const StyledAnchor = styled.a`
  color: #3facff;
  text-decoration: none;
  cursor: pointer;

  &:hover {
    filter: brightness(1.3);
  }
`;

export const StyledQuickReply = styled.button`
  background: ${ThemeQuaternaryBackgroundColor};
  color: ${ThemePrimaryTextColor};
  border: none;
  flex-shrink: 0;
  padding: 0 ${ThemeHorizontalPadding};
  font-size: ${ThemeMediumTextSize};
  cursor: pointer;

  ${buttonHover}
`;

export const StyledPostOptions = styled.div`
  padding: 10px;
  position: relative;

  p {
    margin: ${ThemeVerticalPadding} 0;
  }

  input[type='checkbox'] {
    display: none;
  }

  .options-icon {
    color: #ff5959;
    font-size: 16px;
    font-size: 23px;
    vertical-align: middle;
  }

  label {
    cursor: pointer;
  }

  .options-btn {
    background: ${ThemeQuaternaryBackgroundColor};
    color: ${ThemePrimaryTextColor};
    border: none;
    flex-shrink: 0;
    padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
    cursor: pointer;

    ${buttonHover}
  }

  .option-wrapper {
    margin: ${ThemeVerticalPadding} 0;
    padding: ${ThemeVerticalPadding} 0;

    color: ${ThemePrimaryTextColor};

    &:first-child {
      margin-top: unset;
    }
    &:last-child {
      margin-bottom: unset;
    }
  }
`;

export const PostReplyButtonWrapper = styled.div`
  display: flex;
  @media (max-width: 900px) {
    button {
      padding: 15px 0;
      flex: 1;
    }
  }

  .help-link {
    display: flex;
    justify-content: center;
    align-items: center;
    background: ${ThemeQuaternaryBackgroundColor};
    color: ${ThemePrimaryTextColor};
    border: none;
    flex-shrink: 0;
    padding: 0 ${ThemeHorizontalPadding};
    font-size: ${ThemeMediumTextSize};
    cursor: pointer;

    ${buttonHover}
  }
`;

export const StyledBBPostInput = styled.textarea`
  border: none;
  padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
  background: ${ThemeBackgroundLighter};
  color: ${ThemePrimaryTextColor};
  box-sizing: border-box;
  outline: none;
  width: 100%;
  min-height: 150px;
  line-height: 16px;
`;

export const StyledPostEditorBB = styled.div`
  border-radius: 5px;
  overflow: hidden;
  .post-tools {
    display: flex;

    @media (max-width: 900px) {
      flex-direction: column;
    }
  }
`;
