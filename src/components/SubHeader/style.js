import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { buttonHover, MobileMediaQuery, DesktopMediaQuery } from '../SharedStyles';
import {
  ThemeSecondaryBackgroundColor,
  ThemeTertiaryBackgroundColor,
  ThemeQuaternaryBackgroundColor,
  ThemePrimaryTextColor,
  ThemeHugeTextSize,
  ThemeLargeTextSize,
  ThemeMediumTextSize,
  ThemeVerticalPadding,
  ThemeVerticalHalfPadding,
  ThemeHorizontalPadding,
  ThemeHorizontalHalfPadding,
  ThemeSmallTextSize
} from '../../Theme';
import {StyledForumIcon} from '../ForumIcon';

export const PageTitle = styled.h1`
  margin: ${ThemeVerticalHalfPadding} ${ThemeHorizontalPadding};
  color: ${ThemePrimaryTextColor};
  font-size: ${ThemeHugeTextSize};
  line-height: 1.1;
  overflow-wrap: break-word;
  vertical-align: middle;

  ${StyledForumIcon} {
    margin-right: 5px;
    max-height: 35px;
    vertical-align: middle;
  }
`;

export const SubHeader = styled.header`
  box-sizing: border-box;
  max-width: 100vw;
  padding-top: ${ThemeVerticalPadding};
  padding-bottom: ${ThemeVerticalHalfPadding};
  padding-left: ${ThemeHorizontalPadding};
  padding-right: ${ThemeHorizontalPadding};
  display: flex;
  flex-direction: row;
`;

export const SubHeaderBackAndTitle = styled.span`
  display: flex;
  height: 30px;
  border-radius: 5px;
  overflow: hidden;
  align-items: stretch;
  margin-right: ${ThemeHorizontalPadding};
  background: ${ThemeSecondaryBackgroundColor};
  text-decoration: none;
`;

export const SubHeaderPaginationAndButtons = styled.div`
  display: flex;
  flex-grow: 1;
`;

export const SubHeaderPagination = styled.div`
  display: flex;
  justify-content: flex-end;
  flex-grow: 1;
`;

export const SubHeaderBack = styled(Link)`
  display: block;
  margin: 0;
  font-size: 24px;
  height: 30px;
  padding: 0 ${ThemeHorizontalPadding};
  color: ${ThemePrimaryTextColor};
  background: ${ThemeQuaternaryBackgroundColor};
  text-decoration: none;
  cursor: pointer;

  span {
    font-size: ${ThemeSmallTextSize};
    white-space: nowrap;
    vertical-align: middle;
  }
`;

export const SubHeaderBackText = styled.span`
  padding-left: ${ThemeHorizontalHalfPadding};
  ${MobileMediaQuery} {
    display: none;
  }
`;

export const SubHeaderTitle = styled.h2`
  display: inline-block;
  flex-grow: 1;
  flex-shrink: 1;
  margin: 0;
  padding-left: ${ThemeHorizontalPadding};
  padding-right: ${ThemeHorizontalPadding};
  font-size: ${ThemeLargeTextSize};
  font-weight: 100;
  text-overflow: ellipsis;
  line-height: 30px;
  white-space: nowrap;
  overflow: hidden;
  color: ${ThemePrimaryTextColor};
`;

export const SubHeaderDropdown = styled.a`
  position: relative;
  width: 30px;
  height: 30px;
  border-radius: 5px;
  margin-left: ${ThemeHorizontalPadding};
  overflow: hidden;

  &:active,
  &:focus,
  &:hover {
    overflow: visible;
  }

  ${DesktopMediaQuery} {
    width: auto;
    margin-left: 0;
    overflow: visible !important;
  }
`;

export const SubHeaderDropdownArrow = styled.i`
  position: relative;
  display: block;
  width: 100%;
  height: 30px;
  line-height: 30px;
  text-align: center;
  font-size: ${ThemeMediumTextSize};
  color: ${ThemePrimaryTextColor};
  background: ${ThemeQuaternaryBackgroundColor};
  border-radius: 5px;
  cursor: pointer;
  z-index: 41;

  ${DesktopMediaQuery} {
    display: none;
  }
`;

export const SubHeaderDropdownList = styled.ul`
  position: absolute;
  top: 0;
  right: 0;
  margin: 0;
  list-style: none;
  padding: 32px 0 0 0;
  background: ${ThemeQuaternaryBackgroundColor};
  box-shadow: 0 0 0 2000px rgba(0, 0, 0, 0.5);
  border-radius: 5px;
  z-index: 40;

  ${DesktopMediaQuery} {
    position: relative;
    width: auto;
    padding: 0;
    background: transparent;
    box-shadow: none;
    display: flex;
    flex-direction: row;
  }
`;

export const SubHeaderDropdownListItem = styled.li`
  position: relative;
  list-style: none;
  padding: 0;
  margin: ${ThemeVerticalHalfPadding} ${ThemeHorizontalHalfPadding};

  ${DesktopMediaQuery} {
    margin: 0;
    margin-left: ${ThemeHorizontalPadding};
  }
`;

export const SubHeaderButton = styled(Link)`
  position: relative;
  display: block;
  appearance: none;
  color: ${ThemePrimaryTextColor};
  background: ${ThemeTertiaryBackgroundColor};
  margin: 0;
  padding: 0;
  padding-left: 30px;
  font-size: ${ThemeMediumTextSize};
  text-align: left;
  line-height: 30px;
  border-radius: 5px;
  text-decoration: none;
  white-space: nowrap;
  border: none;
  outline: none;

  i {
    position: absolute;
    top: 0;
    left: 0;
    width: 30px;
    line-height: 30px;
    text-align: center;
    background: ${ThemeSecondaryBackgroundColor};
    border-radius: 3px 0 0 3px;
  }

  span {
    display: block;
    padding: 0 ${ThemeHorizontalHalfPadding};
    height: 30px;
  }

  ${buttonHover}

  ${DesktopMediaQuery} {
    margin: 0;
    background: ${ThemeQuaternaryBackgroundColor};
  }
`;

export const SubHeaderRealButton = styled.button`
  position: relative;
  display: block;
  appearance: none;
  color: ${ThemePrimaryTextColor};
  background: ${ThemeTertiaryBackgroundColor};
  width: 100%;
  margin: 0;
  padding: 0;
  padding-left: 30px;
  font-size: ${ThemeMediumTextSize};
  text-align: left;
  line-height: 30px;
  border-radius: 5px;
  text-decoration: none;
  white-space: nowrap;
  border: none;
  outline: none;

  i {
    position: absolute;
    top: 0;
    left: 0;
    width: 30px;
    line-height: 30px;
    text-align: center;
    background: ${ThemeSecondaryBackgroundColor};
    border-radius: 3px 0 0 3px;
  }

  span {
    display: block;
    padding: 0 ${ThemeHorizontalHalfPadding};
    height: 30px;
  }

  ${buttonHover}

  ${DesktopMediaQuery} {
    margin: 0;
    background: ${ThemeQuaternaryBackgroundColor};

    i {
      border-radius: 3px;
    }

    span {
      max-width: 0;
      overflow: hidden;
      padding: 0;
    }
  }
`;

export const SubHeaderEditButton = styled.button`
  display: block;
  width: 100%;
  color: ${ThemePrimaryTextColor};
  background: transparent;
  margin: 0;
  padding: 0;
  text-align: center;
  font-size: ${ThemeMediumTextSize};
  line-height: 30px;
  text-decoration: none;

  border: none;

  ${buttonHover}
`;
