// @ts-check
import PropTypes from 'prop-types';
import React from 'react';
import styled from 'styled-components';
import { getIcon } from '../../services/icons';

export const StyledForumIcon = styled.img`
  border-radius: 5px;
  max-height: 100%;
  max-width: 100%;
  flex-shrink: 0;
`;

const ForumIcon = ({ iconId }) => {
  const icon = getIcon(iconId);
  return <StyledForumIcon src={icon.url} alt={icon.desc} title={icon.desc} />;
};

ForumIcon.propTypes = {
  iconId: PropTypes.number.isRequired
};

export default ForumIcon;
