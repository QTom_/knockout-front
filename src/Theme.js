import theme from 'styled-theming';

// scale

export const ThemeVerticalPadding = theme('scale', {
  large: '10px',
  medium: '8px',
  small: '6px'
});

export const ThemeVerticalHalfPadding = theme('scale', {
  large: '5px',
  medium: '4px',
  small: '3px'
});

export const ThemeHorizontalPadding = theme('scale', {
  large: '15px',
  medium: '12px',
  small: '8px'
});

export const ThemeHorizontalHalfPadding = theme('scale', {
  large: '7.5px',
  medium: '6px',
  small: '4px'
});

export const ThemeHugeTextSize = theme('scale', {
  large: '42px',
  medium: '32px',
  small: '24px'
});

export const ThemeLargeTextSize = theme('scale', {
  large: '18px',
  medium: '14.5px',
  small: '12.5px'
});

export const ThemeMediumTextSize = theme('scale', {
  large: '17px',
  medium: '13.5px',
  small: '11.5px'
});

export const ThemeSmallTextSize = theme('scale', {
  large: '14px',
  medium: '12.5px',
  small: '10.5px'
});

// width

export const ThemeBodyWidth = theme('width', {
  full: 'auto',
  verywide: '1920px',
  wide: '1440px',
  medium: '1280px',
  narrow: '960px'
});

// logo

export const ThemeMainLogo = theme('mode', {
  light: '/static/logo_light.png',
  dark: '/static/logo_dark.png'
});

// color

export const ThemePrimaryBackgroundImage = theme('mode', {
  light: 'url(/static/bg_light.jpg)',
  dark: 'url(/static/bg_dark.jpg)'
});

export const ThemePrimaryBackgroundColor = theme('mode', {
  light: 'rgba(0,0,0,0.025)',
  dark: 'rgba(0,0,0,0.9)'
});

export const ThemeBodyColor = theme('mode', {
  light: 'rgba(0,0,0,0.05)',
  dark: 'rgba(0,0,0,0.1)'
});

export const ThemeBodyColorWithBg = theme('mode', {
  light: 'rgba(255,255,255,0.4)',
  dark: 'rgba(0,0,0,0.4)'
});

export const ThemeSecondaryBackgroundColor = theme('mode', {
  light: '#f0f2f3',
  dark: '#222226'
});

export const ThemeTertiaryBackgroundColor = theme('mode', {
  light: '#ebedef',
  dark: '#28282c'
});

export const ThemeQuaternaryBackgroundColor = theme('mode', {
  light: '#e6e6e6',
  dark: '#2d2d30'
});

export const ThemeNavTextColor = theme('mode', {
  light: '#222',
  dark: '#fff'
});

export const ThemeFooterTextColor = theme('mode', {
  light: '#222',
  dark: '#fff'
});

export const ThemePrimaryTextColor = theme('mode', {
  light: '#222',
  dark: '#fff'
});

export const ThemePrimaryTextColorInverted = theme('mode', {
  light: '#fff',
  dark: '#222'
});

export const ThemeSecondaryTextColor = theme('mode', {
  light: '#333',
  dark: '#aaa'
});

export const ThemeTertiaryTextColor = theme('mode', {
  light: '#232323',
  dark: '#fff'
});

export const ThemeHoverFilter = theme('mode', {
  light: 'brightness(97%)',
  dark: 'brightness(125%)'
});

export const ThemeHoverShadow = theme('mode', {
  light: 'inset 0px 0px 3px 0px rgba(0, 0, 0, 0.1)',
  dark: 'inset 0px 0px 3px 0px rgba(255, 255, 255, 0.2)'
});

export const ThemeBackgroundImageGradient = theme('mode', {
  light: 'linear-gradient(to bottom,rgba(150,150,150,0.65) 0px,#e6e6e6 460px)',
  dark: 'linear-gradient(to bottom,rgba(45,45,48,0.65) 0px,#2d2d30 460px)'
});

export const ThemeBackgroundImageGradientHover = theme('mode', {
  light: 'linear-gradient(to bottom,rgba(230,230,230,0) 350px,#e6e6e6 460px)',
  dark: 'linear-gradient(to bottom,rgba(45,45,48,0) 350px,#2d2d30 460px)'
});

export const ThemeLockedColor = theme('mode', {
  light: '#1f1818',
  dark: '#1f1818'
});

export const ThemeScrollbarHandle = theme('mode', {
  light: '#666',
  dark: '#666'
});

export const ThemeScrollbarHandleHover = theme('mode', {
  light: '#888',
  dark: '#ddd'
});

export const ThemeGoldMemberColor = theme('mode', {
  light: 'linear-gradient(375deg,#ff841b,#e6ae1e,#ffbc00,#ffb701,#fcbe20)',
  dark: 'linear-gradient(375deg, #fcbe20, #fcbe20, #ffe770, #fcbe20, #fcbe20)'
});

export const ThemeGoldMemberGlow = theme('mode', {
  light: `drop-shadow(rgba(100, 90, 50, 0.4) 0px 0px 0px) drop-shadow(rgba(200, 120, 70, 0.8) 1px 1px 0px) drop-shadow(rgba(230, 174, 30, 0.2) 1px 1px 0.05em))`,
  dark: 'drop-shadow(0px 0px 2px #ffcc4a)'
});

export const ThemeModeratorExtraStyle = theme('mode', {
  light: 'drop-shadow(0px 0px 2px rgba(0,60,20,0.7));',
  dark: 'drop-shadow(0px 0px 0px rgba(255, 231, 112, 0));'
});

const halloween = new Date().getMonth() === 9;

export const ThemeKnockoutRed = theme('mode', {
  light: '#ec3737',
  dark: halloween ? '#ec7337' : '#ec3737'
});

// v2

export const ThemeBackgroundLighter = theme('mode', {
  dark: '#303134',
  light: '#fff'
});
export const ThemeBackgroundDarker = theme('mode', {
  dark: '#232426',
  light: '#eee'
});
export const ThemeBackgroundPage = theme('mode', {
  dark: '#1c1b1b',
  light: '#1c1b1b'
});

export const ThemeTextShadowLightBackground = theme('mode', {
  dark: ' 1px 1px 4px rgba(0, 0, 0, 0.3)',
  light: ' 1px 1px 1px rgba(0, 0, 0, 0.02)'
});
