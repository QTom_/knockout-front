/* eslint-disable react/forbid-prop-types */
import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import ratingList from '../../../components/Rating/ratingList.json';
import { ThemePrimaryTextColor } from '../../../Theme';

const Paragraph = styled.p`
  color: ${ThemePrimaryTextColor};
`;

const RatingText = styled.div`
  color: ${ThemePrimaryTextColor};
`;

class UserProfileRatings extends React.Component {
  handleToggleContent = key => {
    this.setState(prevState => ({ [key]: !prevState[key] }));
  };

  render() {
    const { ratings } = this.props;

    if (ratings.length === 0) {
      return <Paragraph>You have no ratings... how?</Paragraph>;
    }

    return (
      <table>
        <tr>
          <th />
          <th />
        </tr>
        {ratings
          .sort((a, b) => (a.count > b.count ? -1 : 1))
          .map((rating, i) => (
            <tr key={rating}>
              <td>
                <img
                  style={{
                    maxWidth: i === 0 ? 'unset' : '25px',
                    maxHeight: i === 0 ? 'unset' : '25px',
                    verticalAlign: 'middle',
                    paddingRight: i === 0 ? '10px' : '5px'
                  }}
                  src={ratingList[rating.name].url}
                  alt={ratingList[rating.name].name}
                />
              </td>
              <td>
                <pre
                  style={{
                    fontSize: i === 0 ? '20px' : 'unset'
                  }}
                >
                  <RatingText>
                    <b>{ratingList[rating.name].name}</b> x<b>{rating.count}</b>
                  </RatingText>
                </pre>
              </td>
            </tr>
          ))}
      </table>
    );
  }
}

UserProfileRatings.propTypes = {
  ratings: PropTypes.shape({
    name: PropTypes.string.isRequired,
    url: PropTypes.string.isRequired
  }).isRequired
};

export default UserProfileRatings;
