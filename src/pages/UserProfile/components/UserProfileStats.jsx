import React from 'react';
import PropTypes from 'prop-types';

import {
  Grid,
  Tile,
  TileHeader,
  TileBody,
  TileBodyCount
} from '../../../components/Moderate/Dashboard/style';

const UserProfileStats = ({ threadCount, postCount }) => (
  <Grid>
    <Tile>
      <TileHeader>Total posts</TileHeader>
      <TileBody>
        <TileBodyCount>{postCount}</TileBodyCount>
      </TileBody>
    </Tile>
    <Tile>
      <TileHeader>Total threads</TileHeader>
      <TileBody>
        <TileBodyCount>{threadCount}</TileBodyCount>
      </TileBody>
    </Tile>
  </Grid>
);

UserProfileStats.propTypes = {
  threadCount: PropTypes.number.isRequired,
  postCount: PropTypes.number.isRequired
};

export default UserProfileStats;
