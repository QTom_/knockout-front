import React from 'react';
import PropTypes from 'prop-types';

import {
  StyledModalWrapper,
  StyledSectionDelimiter,
  StyledModalContent,
  StyledModalTitle,
  StyledModalIcon
} from '../../../components/Modals/style';
import { SubHeaderRealButton } from '../../../components/SubHeader/style';
import { AbsoluteBlackout } from '../../../components/SharedStyles';
import { updateUserProfile } from '../../../services/user';

class UserProfileEditor extends React.Component {
  state = {
    headingText: null,
    personalSite: null,
    backgroundUrl: null,
    backgroundType: 'cover',
    backgroundImage: null
  };

  handleChange = ({ target }) => {
    if (target.files && target.files[0]) {
      return this.setState({ [target.name]: target.files[0] });
    }

    return this.setState({ [target.name]: target.value });
  };

  async handleSubmit() {
    await updateUserProfile({ ...this.state });
    this.props.closeFn();
  }

  render() {
    const { closeFn } = this.props;

    return (
      <>
        <StyledModalWrapper height="unset">
          <StyledSectionDelimiter>
            <StyledModalIcon
              src="https://img.icons8.com/color/96/000000/paint-net.png"
              alt="Background icon"
            />
            <StyledModalTitle>Edit your profile</StyledModalTitle>
          </StyledSectionDelimiter>

          <StyledModalContent>
            <div>
              <p>
                <b>Heading text:</b>
              </p>
              <input name="headingText" type="text" onChange={e => this.handleChange(e)} />
            </div>
            <br />
            <div>
              <p>
                <b>Personal site URL:</b>
              </p>
              <input name="personalSite" type="text" onChange={e => this.handleChange(e)} />
            </div>
            <br />
            <div>
              <p>
                <b>Background type:</b>
              </p>
              <div>
                <div>
                  <input
                    type="radio"
                    id="bgType1"
                    name="backgroundType"
                    value="cover"
                    checked={this.state.backgroundType === 'cover'}
                    onChange={e => this.handleChange(e)}
                  />
                  <label htmlFor="bgType1">
                    <div className="labels">
                      <i className="fas fa-check-square checked" />
                      <i className="fas fa-square unchecked" />
                      Cover
                    </div>
                  </label>
                </div>
                <div>
                  <input
                    type="radio"
                    id="bgType2"
                    name="backgroundType"
                    value="tiled"
                    checked={this.state.backgroundType === 'tiled'}
                    onChange={e => this.handleChange(e)}
                  />
                  <label htmlFor="bgType2">
                    <div className="labels">
                      <i className="fas fa-check-square checked" />
                      <i className="fas fa-square unchecked" />
                      Tiled
                    </div>
                  </label>
                </div>
              </div>
            </div>
            <br />
            <div>
              <p>
                <b>Background image (max size ~2MB):</b>
              </p>
              <input name="backgroundImage" type="file" onChange={e => this.handleChange(e)} />
            </div>
          </StyledModalContent>

          <StyledSectionDelimiter>
            <SubHeaderRealButton type="button" onClick={closeFn}>
              Cancel
            </SubHeaderRealButton>

            <SubHeaderRealButton type="button" onClick={() => this.handleSubmit()}>
              Submit
            </SubHeaderRealButton>
          </StyledSectionDelimiter>
        </StyledModalWrapper>

        <AbsoluteBlackout />
      </>
    );
  }
}

UserProfileEditor.propTypes = {
  closeFn: PropTypes.func.isRequired
};
export default UserProfileEditor;
