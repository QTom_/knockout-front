/* eslint-disable react/jsx-one-expression-per-line */
import React from 'react';
import styled from 'styled-components';
import { Redirect, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { updateUser } from '../services/user';

import { TextInput, DefaultBlueHollowButton } from '../components/SharedStyles';

import { ThemeHorizontalPadding } from '../Theme';
import { userUpdate } from '../state/user/actions';
import { pushSmartNotification } from '../utils/notification';
import PrivacyPolicy from './PrivacyPolicy/components/PrivacyPolicy';

const submitUser = async ({ event, history, updateLocalUser, user }) => {
  event.preventDefault();

  const username = event.target.username.value;

  if (!username || username.length < 3 || username.length > 20) {
    return pushSmartNotification({
      error: 'Set a proper username. 3 characters or more, less than 20.',
    });
  }

  try {
    await updateUser({ username });

    const updatedUser = { ...user, username, requiresSetup: false };
    updateLocalUser(updatedUser);

    return history.push('/');
  } catch (err) {
    pushSmartNotification({
      error: 'You either used an invalid username or that username is already in use.',
    });
    return console.error(err);
  }
};

const UserSetupWrapper = styled.section`
  position: relative;
  max-width: 480px;
  margin: 100px auto;
  padding: 32px 15px 10px;
  background: rgba(255, 255, 255, 0.03);
  border-radius: 5px;
  line-height: 1.5;

  h2 {
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    margin: 0;
    padding: 0 15px;
    height: 32px;
    font-size: 16px;
    font-weight: 100;
    line-height: 32px;
    border-radius: 5px 5px 0 0;
    background: rgba(255, 255, 255, 0.03);
  }

  p {
    margin: 10px 0 10px 0;
  }

  .input {
    position: relative;
    display: flex;
    flex-direction: row;
    height: 32px;
    line-height: 32px;
    margin-bottom: 10px;
    border: 1px solid gray;
    border-radius: 5px;
    font-family: monospace;
  }

  .input label {
    padding: 0 10px;
    min-width: 75px;
    font-size: 12px;
    flex-grow: 0;
    text-align: center;
    background: rgba(255, 255, 255, 0.1);
  }

  .input input {
    flex-grow: 1;
    border: none;
    margin: 0 0 0 10px;
    padding: 0;
    height: 32px;
    line-height: 32px;
    outline: none;
    font-size: 14px;
    font-family: monospace;
  }

  span {
    display: block;
    margin-bottom: 10px;
    font-size: 12px;
    color: #aaa;
  }

  button {
    width: 100%;
    margin-bottom: 0;
  }
`;

const PageWrapper = styled.section`
  padding-top: 0;
  padding-bottom: 0;
  padding-left: ${ThemeHorizontalPadding};
  padding-right: ${ThemeHorizontalPadding};
  margin: 0 auto;
`;

const UserSetup = (props) => {
  if (!props.user.requiresSetup) {
    return <Redirect to="/" />;
  }

  return (
    <PageWrapper>
      <PrivacyPolicy />

      <p>---</p>
      <h3>
        You should also read the&nbsp;
        <u>
          <a target="_blank" href="/rules">
            Rules
          </a>
        </u>
        .
      </h3>

      <UserSetupWrapper>
        <h2>User Setup</h2>

        <p>Choose a username below.</p>

        <form
          onSubmit={(e) =>
            submitUser({
              event: e,
              history: props.history,
              updateLocalUser: props.userUpdate,
              user: props.user,
            })
          }
        >
          <div className="input">
            <label>Username</label>
            <TextInput type="text" name="username" placeholder="ThreadKillerXxX" />
          </div>

          <p>You will be able to set up an avatar later by going into your settings page.</p>

          <DefaultBlueHollowButton type="submit">Submit</DefaultBlueHollowButton>
        </form>
      </UserSetupWrapper>
    </PageWrapper>
  );
};

UserSetup.propTypes = {
  user: PropTypes.shape({
    username: PropTypes.string,
    requiresSetup: PropTypes.bool,
  }),
  history: PropTypes.shape({
    push: PropTypes.func.isRequired,
  }).isRequired,
  userUpdate: PropTypes.func.isRequired,
};

UserSetup.defaultProps = {
  user: {
    username: undefined,
    requiresSetup: false,
  },
};

const mapStateToProps = ({ user }) => ({
  user,
});

export default withRouter(connect(mapStateToProps, { userUpdate })(UserSetup));
