import styled from 'styled-components';
import { buttonHoverBrightness } from '../../../components/SharedStyles';
import { ThreadPageItem } from '../../SubforumPage/components/style';

import { ThemeVerticalHalfPadding, ThemeHorizontalHalfPadding } from '../../../Theme';

export const DeleteAlertButton = styled.button`
  cursor: pointer;
  color: white;
  background: rgb(187, 37, 37);
  width: 30px;
  margin-left: ${ThemeHorizontalHalfPadding};
  border: none;
  border-radius: 5px;

  ${buttonHoverBrightness}
`;

export const ThreadItemWrapper = styled.div`
  display: flex;
  margin: ${ThemeVerticalHalfPadding} 0;

  ${ThreadPageItem} {
    margin: 0;
  }
`;
