import React, { useState } from 'react';
import styled from 'styled-components';
import {
  SubHeaderDropdownListItem,
  SubHeaderRealButton
} from '../../../components/SubHeader/style';
import Tooltip from '../../../components/Tooltip';
import { ThreadStatusAction } from './style';
import { getTags, updateThreadTags } from '../../../services/tags';
import { AbsoluteBlackout } from '../../../components/SharedStyles';
import {
  ThemePrimaryTextColor,
  ThemeSecondaryBackgroundColor,
  ThemeKnockoutRed
} from '../../../Theme';
import {
  StyledSectionDelimiter,
  StyledModalIcon,
  StyledModalTitle,
  StyledModalContent,
  StyledModalSelect
} from '../../../components/Modals/style';
import { pushSmartNotification } from '../../../utils/notification';

const getTagName = str => str.split('|')[1];
const getTagId = str => str.split('|')[0];

const StyledTagsModal = styled.div`
  position: fixed;
  left: 50%;
  top: 50%;
  -webkit-font-smoothing: subpixel-antialiased;
  transform: translate(-50%, -50%);

  max-width: 100vw;
  width: 500px;
  height: ${props => props.height || '250px'};
  max-height: 100vh;

  display: flex;
  flex-direction: column;
  justify-content: space-between;

  background: ${ThemeSecondaryBackgroundColor};
  color: ${ThemePrimaryTextColor};
  border-radius: 5px;

  z-index: 100;
`;

const Tag = styled.button`
  padding: 3px 6px;
  margin: 0 5px;
  background: ${ThemeKnockoutRed};
  border: none;
  border-radius: 10px;
  color: white;
  font-size: 10px;
  line-height: 10px;
  height: min-content;
  display: inline-block;
  cursor: pointer;

  &:hover {
    filter: brightness(1.25);
  }
`;

const ModalWrapper = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100vw;
  height: 100vh;
`;

const TagButton = ({ thread }) => {
  const [isVisible, toggleIsVisible] = useState(false);
  const [availableTags, setAvailableTags] = useState([]);
  const [selectedTags, setSelectedTags] = useState([]);

  const toggleVisible = async () => {
    toggleIsVisible(!isVisible);

    if (availableTags.length === 0) {
      const tags = await getTags();

      setAvailableTags(tags);
    }
  };

  const handleTagAdd = e => {
    if (selectedTags.length === 3) {
      return;
    }
    const { value } = e.target;

    setSelectedTags([...selectedTags, value]);
  };

  const handleTagRemove = index => {
    const newTags = [...selectedTags];
    newTags.splice(index, 1);

    setSelectedTags(newTags);
  };

  const handleSubmit = async () => {
    const payload = {
      threadId: thread.id,
      tags: selectedTags.map(tag => getTagId(tag))
    };

    try {
      await updateThreadTags(payload);

      pushSmartNotification({ message: 'Tags updated.' });

      toggleIsVisible();
    } catch (error) {
      pushSmartNotification({ error: 'Could not update tags.' });
    }
  };

  if (!isVisible) {
    return (
      <SubHeaderDropdownListItem>
        <Tooltip text="Edit tags">
          <ThreadStatusAction active onClick={toggleVisible}>
            <i className="fas fa-tag" />
            <span>Edit tags</span>
          </ThreadStatusAction>
        </Tooltip>
      </SubHeaderDropdownListItem>
    );
  }

  return (
    <ModalWrapper>
      <StyledTagsModal>
        <StyledSectionDelimiter>
          <StyledModalIcon src="/static/icons/tag.png" alt="Background icon" />
          <StyledModalTitle>Tags</StyledModalTitle>
        </StyledSectionDelimiter>

        <StyledModalContent>
          <div className="heading">
            <p>Tags:</p>
          </div>
          <div className="input tall tags-list">
            <StyledModalSelect id="tag-list" onChange={handleTagAdd} value="default">
              <option value="default">Select a tag...</option>
              {availableTags.map(tag => (
                <option
                  key={tag}
                  value={`${tag.id}|${tag.name}`}
                  disabled={selectedTags.includes(`${tag.id}|${tag.name}`)}
                >
                  {tag.name}
                </option>
              ))}
            </StyledModalSelect>
          </div>
          <div className="input">
            <label className="inputLabel">
              <p>Selected tags:</p>
            </label>
            <div className="selected-tags">
              {selectedTags.map((tag, index) => (
                <Tag
                  key={tag}
                  type="button"
                  className="tag-item"
                  onClick={() => handleTagRemove(index)}
                >
                  <i className="fas fa-times-circle" />
                  &nbsp;
                  {typeof tag === 'string' ? getTagName(tag) : '?'}
                </Tag>
              ))}
            </div>
          </div>
        </StyledModalContent>

        <StyledSectionDelimiter>
          <SubHeaderRealButton type="button" onClick={toggleVisible}>
            Cancel
          </SubHeaderRealButton>

          <SubHeaderRealButton type="button" onClick={handleSubmit}>
            Submit
          </SubHeaderRealButton>
        </StyledSectionDelimiter>
      </StyledTagsModal>
      <AbsoluteBlackout />
    </ModalWrapper>
  );
};

export default TagButton;
