/* eslint-disable react/jsx-one-expression-per-line */
import React from 'react';

import {
  SubHeader,
  SubHeaderBack,
  SubHeaderBackAndTitle,
  SubHeaderTitle
} from '../../components/SubHeader/style';
import { RulesWrapper } from './components/style';
import RulesSwitcher from './components/RulesSwitcher';

const RulesPage = () => (
  <>
    <SubHeader>
      <SubHeaderBackAndTitle>
        <SubHeaderBack to="/">&#8249;</SubHeaderBack>
        <SubHeaderTitle>Rules</SubHeaderTitle>
      </SubHeaderBackAndTitle>
    </SubHeader>
    <RulesWrapper>
      <RulesSwitcher />
    </RulesWrapper>
  </>
);

export default RulesPage;
