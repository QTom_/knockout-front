import React, { Component } from 'react';
import Rules from './Rules';
import PrivacyPolicy from '../../PrivacyPolicy/components/PrivacyPolicy';
import ModeratorCoc from './ModeratorCoc';
import { UserProfileTabContainer, UserProfileTab } from '../../UserProfile/components/style';

export default class RulesSwitcher extends Component {
  state = {
    currentTab: 0
  };

  renderTab = () => {
    switch (this.state.currentTab) {
      case 0:
        return <Rules />;

      case 1:
        return <PrivacyPolicy />;

      case 2:
        return <ModeratorCoc />;

      default:
        return null;
    }
  };

  handleTabSwitch = tabId => this.setState({ currentTab: tabId });

  render() {
    const { currentTab } = this.state;

    return (
      <div>
        <UserProfileTabContainer>
          <UserProfileTab isSelected={currentTab === 0} onClick={() => this.handleTabSwitch(0)}>
            <i className="fas fa-atlas" /> Rules
          </UserProfileTab>
          <UserProfileTab isSelected={currentTab === 1} onClick={() => this.handleTabSwitch(1)}>
            <i className="fas fa-balance-scale" /> Privacy Policy
          </UserProfileTab>
          <UserProfileTab isSelected={currentTab === 2} onClick={() => this.handleTabSwitch(2)}>
            <i className="fas fa-gavel" /> Moderator Code of Conduct
          </UserProfileTab>
        </UserProfileTabContainer>

        {this.renderTab()}
      </div>
    );
  }
}
