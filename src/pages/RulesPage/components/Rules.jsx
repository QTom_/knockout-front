import React from 'react';

const Rules = () => (
  <>
    <ol>
      <li>
        <b>Use English</b> - Certain threads maybe multilingual when directed however English is the
        most used language on the forums.
      </li>
      <li>
        <b>Read before you post</b> - Please read what the thread is about and what users have said
        before making your post. Sometimes you may end up repeating what someone else has said and
        that’s okay. But it is not okay to ignore the OP.
      </li>
      <li>
        <b>Thread Necromancy</b> - Don’t bump threads unless you’re adding content or requesting
        status updates on projects. Going “any news” is not okay.
      </li>
      <li>
        <b>Warez</b> - Don’t link. You can discuss piracy but don’t brag about pirating X thing.
        Posting Abandonware is okay if you legally cannot purchase said content. Check My
        Abandonware (
        <a href="https://www.myabandonware.com" target="_blank" rel="noopener">
          https://www.myabandonware.com
        </a>
        ) before posting.
      </li>
      <li>
        <b>Don’t flame</b> - Don’t flame - Refrain from calling someone an idiot or insulting them
        directly (no matter how benign the insult). Even if they’re being an idiot. Be tactful and
        respectful when talking to other users, even if you disagree with them.
      </li>
      <li>
        <b>Don’t have a NSFW avatar</b> - Don’t use avatars that are or contain the following: Porn,
        suggestive, cropped, blurred. Basically think of how a “middle aged christian soccer mom
        boss” would think if they saw it. If you skirt the borderlines you will be contacted by a
        moderator to find a compromise. Moderators also reserve the right to remove your avatar and
        background. We don’t care what it is as long as it doesn’t break the above.
      </li>
      <li>
        <b>Don’t troll or uphold bad faith arguments</b> - Don’t be vague, piss around, take no care
        in posting, post in bad faith to irritate your fellow users. We won’t give out examples. But
        we will catch you if you break this.
      </li>
      <li>
        <b>Report</b> - Report posts that break any of the rules. Don’t report people because you
        disagree with them.
      </li>
      <li>
        <b>Bigotry</b> - It’s not welcomed here. Period. No racism. No sexism. No phobia.
      </li>
      <li>
        <b>Thread OP Rules</b> - Some threads have their own specific set of rules in the OP. Please
        do read them.
      </li>
      <li>
        <b>Sobriety</b> - You won’t get banned if you are under the influence. But you shouldn’t let
        it disrupt the flow of threads and posts.
      </li>
      <li>
        <b>Callouts</b> - Don’t do this. It’s obnoxious and doesn’t contribute in anyway.
      </li>
      <li>
        <b>Don’t be a dick</b> - This doesn’t need explaining.
      </li>
      <li>
        <b>Raiding</b> - Don’t even think about doing this or encouraging this. Raiding other
        places/discord servers/other forums to cause a mess is going to get you banned there and
        here. It’s not tolerated. Period.
      </li>
      <li>
        <b>Advocating violence</b> - Do not advocate for violence or harm towards anyone. There is
        zero tolerance for that behavior. If you&apos;re angry at someone, you can express it in
        other ways. Levelheadedness is expected and required of every user.
      </li>
      <li>
        <b>NSFW Content</b> - All NSFW content must be limited to threads tagged NSFW. Do not post
        NSFW content outside properly tagged threads. If you&apos;re creating a thread that
        does/might contain NSFW content, tag it so or you will be awarded a severe ban.
      </li>
    </ol>

    <h2>Important</h2>

    <p>
      There are times where moderators will use their discretion and common sense to maintain a
      neutral, unbiased and fair stance. Think we’re not doing our job? Problem with a ban? You can
      contact us on the Discord. Remember. Bans are warnings. You are more than welcome to get an
      explanation if you don’t understand the warning.
    </p>

    <h2>News Rules</h2>

    <ol>
      <li>
        <b>Keep it fresh</b> - Sources should be less than 2 weeks old.
      </li>
      <li>
        <b>Don’t editoralise</b> - You should also not change the title too much to a point where it
        doesn’t fit the original title(this prevents click baity headlines but throws responsibility
        on the OP to get it right, use your judgement)
      </li>
      <li>
        <b>Report</b> - Report posts that break any of the rules. Don’t report people because you
        disagree with them.
      </li>
      <li>
        <b>Keep a cool head</b> - If things get heated take a step back.(I don’t expect this rule to
        be used often or at all but expect us mods to roll into a thread and tell people too chill)
      </li>
      <li>
        <b>Be funny when it’s appropriate</b> - You can crack epic zingers and jokes but in national
        tragedies and high profile stories we expect you to keep them to yourselves. (aka don’t make
        jokes about dead people)
      </li>
      <li>
        <b>Don’t post manifestos and/or stuff to hate rhetorics</b> - We don’t want to harbor or be
        a source.
      </li>
      <li>
        <b>BREAKING</b> - Don’t put breaking in your titles. Us moderators will make that call.
      </li>
      <li>
        <b>Updating</b> - If you are the op. It will be useful if you kept your OP full of updates
        linking to posts during high profile, breaking headlines.
      </li>
      <li>
        <b>Opinions</b> - Opinion pieces are not news.
      </li>
      <li>
        <b>Satire</b> - Satire pieces are not okay either. Don’t use tabloids either.
      </li>
      <li>
        <b>Paywall</b> - Don’t post paywall articles. If you must, you have to present an
        alternative source as well your paywall.
      </li>
    </ol>

    <h2>Politics Rules</h2>

    <ol>
      <li>
        <b>Keep it fresh</b> - Sources should be less than 2 weeks old.
      </li>
      <li>
        <b>Do not editoralise</b> - You should also not change the title too much to a point where
        it doesn’t fit the original title(this prevents click baity headlines but throws
        responsibility on the OP to get it right, use your judgement)
      </li>
      <li>
        <b>Report</b> - Report posts that break any of the rules. Don’t report people because you
        disagree with them.
      </li>
      <li>
        <b>Keep a cool head</b> - If things get heated take a step back.(I don’t expect this rule to
        be used often or at all but expect us mods to roll into a thread and tell people too chill)
      </li>
      <li>
        <b>Keep it classy and informative</b> - Maintain a high quality posting standard. Don’t
        shitpost. If you crack a joke you run the risk of being banned(in other words, don’t piss
        off the entire thread with your sick epic one liner).
      </li>
      <li>
        <b>Be factual</b> - Moderators reserve the right to challenge your bold claims. If
        challenged you WILL be expected to follow up with your source and it needs to be fact.
      </li>
      <li>
        <b>Neutrality</b> - When creating a thread. Use the most neutral and factual source. We
        aren’t going to make a list or use a premade one. We expect you to use your judgement here.
        It is safer to follow up with an additional source as your backup if your original is bogus.
      </li>
      <li>
        <b>Honesty</b> - Don’t be wishy washy, vague or up hold bad faith. You will piss off
        everyone and you will be banned.
      </li>
      <li>
        <b>Image Macros</b> - One per page per user. There are no exceptions.
      </li>
      <li>
        <b>Opinions</b> - You can make opinion piece threads but you must post a relevant source
        that the opinion article is talking about AND you&apos;re allowed broad/general opinions.
        You won’t be banned unless you intentionally break this.
      </li>
      <li>
        <b>Satire</b> - Satire pieces are not okay either. Don’t use tabloids either.
      </li>
      <li>
        <b>Paywall</b> - Don’t post paywall articles. If you must, you have to present an
        alternative source as well your paywall.
      </li>
    </ol>
  </>
);

export default Rules;
