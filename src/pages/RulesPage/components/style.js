import styled from 'styled-components';

import {
  ThemeVerticalPadding,
  ThemeHorizontalPadding,
  ThemePrimaryTextColor,
  ThemeSecondaryTextColor
} from '../../../Theme';

export const RuleList = styled.div`
  padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
`;

export const RulesWrapper = styled.section`
  padding: 0 15px;
  border-radius: 0 0 5px 5px;
  margin: 0 auto;
  color: ${ThemePrimaryTextColor};

  line-height: 1.3;

  p {
    margin-bottom: 15px;
    line-height: 1.5;
  }

  ul,
  ol {
    padding-left: 20px;
  }

  li {
    margin-bottom: 5px;
  }

  li a {
    color: #3facff;
  }

  h2 {
    font-weight: 100;
    font-size: 28px;
    margin-top: 25px;
    margin-bottom: 10px;
    color: ${ThemeSecondaryTextColor};

    &:nth-child(1) {
      margin-top: 0;
    }
  }

  h3 {
    font-weight: 100;
    font-size: 22px;
    margin-top: 25px;
    margin-bottom: 10px;
    color: ${ThemeSecondaryTextColor};
  }
`;
