import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import uploadAvatar, { uploadBackground } from '../../services/avatar';
import {
  loadUserFromStorage,
  updateUser,
  updateProfileRatingsDisplay,
  getProfileRatingsDisplay,
} from '../../services/user';
import LoggedInOnly from '../../components/LoggedInOnly';
import config from '../../../config';

import {
  loadThemeFromStorage,
  loadWidthFromStorage,
  loadScaleFromStorage,
  setThemeToStorage,
  setWidthToStorage,
  setScaleToStorage,
  loadAutoSubscribeFromStorage,
  setAutoSubscribeToStorage,
  setRatingsXrayToStorage,
  loadRatingsXrayFromStorage,
  loadStickyHeaderFromStorageBoolean,
  setStickyHeaderToStorage,
  loadPunchyLabsFromStorageBoolean,
  setPunchyLabsToStorage,
} from '../../services/theme';

import {
  SubHeader,
  SubHeaderBackAndTitle,
  SubHeaderPaginationAndButtons,
  SubHeaderPagination,
  SubHeaderDropdown,
  SubHeaderDropdownArrow,
  SubHeaderDropdownList,
  SubHeaderDropdownListItem,
  SubHeaderTitle,
  SubHeaderBack,
  SubHeaderButton,
} from '../../components/SubHeader/style';

import {
  ThemeVerticalPadding,
  ThemeVerticalHalfPadding,
  ThemeHorizontalPadding,
  ThemeHorizontalHalfPadding,
  ThemeMediumTextSize,
  ThemeSmallTextSize,
  ThemePrimaryBackgroundColor,
  ThemeSecondaryBackgroundColor,
  ThemeTertiaryBackgroundColor,
  ThemeQuaternaryBackgroundColor,
  ThemePrimaryTextColor,
  ThemeSecondaryTextColor,
} from '../../Theme';
import { DefaultBlueHollowButton } from '../../components/SharedStyles';
import { pushSmartNotification } from '../../utils/notification';
import Tooltip from '../../components/Tooltip';
import LoginButton from '../../components/LoginButton';
import UserGroupRestricted from '../../components/UserGroupRestricted';
import {
  saveDisplayCountryInfoSettingToStorage,
  loadDisplayCountrySettingFromStorage,
  loadDisplayNsfwFilterSettingFromStorage,
  saveDisplayNsfwFilterSettingToStorage,
  loadDisplayNsfwFilterSettingFromStorageBoolean,
} from '../../utils/postOptionsStorage';
import DeleteAccountWarning, { StyledDeleteAccountButton } from './components/DeleteAccount';
import { scrollToTop } from '../../utils/pageScroll';

const PageWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: stretch;
  padding: ${ThemeVerticalHalfPadding} ${ThemeHorizontalHalfPadding};
  flex-wrap: wrap;

  .column {
    display: flex;
    flex-grow: 1;
    flex-direction: column;
  }
`;

const Panel = styled.div`
  position: relative;
  flex-grow: 1;
  flex-shrink: 0;
  min-width: 320px;
  margin: ${ThemeVerticalHalfPadding} ${ThemeHorizontalHalfPadding};
  padding-bottom: ${ThemeVerticalPadding};
  background: ${ThemeSecondaryBackgroundColor};
  border-radius: 5px;
  line-height: 1.5;

  ${(props) => props.flexCenter && 'display: flex; justify-content: center; align-items: center;'}

  h2 {
    position: relative;
    padding-top: ${ThemeVerticalHalfPadding};
    padding-bottom: ${ThemeVerticalHalfPadding};
    padding-left: ${ThemeHorizontalPadding};
    padding-right: ${ThemeHorizontalPadding};
    margin-top: 0;
    margin-bottom: ${ThemeVerticalPadding};
    font-size: ${ThemeMediumTextSize};
    font-weight: 100;
    line-height: 1.4;
    text-align: center;
    color: ${ThemePrimaryTextColor};
    background: ${ThemeTertiaryBackgroundColor};
    border-radius: 5px 5px 0 0;
  }

  label.avatar {
    position: relative;
    display: block;
    margin-top: ${ThemeVerticalPadding};
    margin-bottom: ${ThemeVerticalPadding};
    margin-left: auto;
    margin-right: auto;
    background: rgba(0, 0, 0, 0.1);
  }

  label.avatar.fg {
    width: 115px;
    height: 115px;
  }

  label.avatar.bg {
    width: 230px;
    height: 460px;
  }

  label.avatar i {
    position: absolute;
    display: inline-block;
    top: 50%;
    left: 50%;
    width: 32px;
    line-height: 32px;
    margin-top: -16px;
    margin-left: -16px;
    text-align: center;
    opacity: 0.5;
    border-radius: 100%;
    color: ${ThemePrimaryTextColor};
    background: ${ThemePrimaryBackgroundColor};
  }

  input#upload {
    display: none;
  }

  input#bgupload {
    display: none;
  }

  .input {
    position: relative;
    display: flex;
    flex-direction: row;
    border-radius: 5px;

    margin-top: ${ThemeVerticalPadding};
    margin-bottom: ${ThemeVerticalPadding};
    margin-left: ${ThemeHorizontalPadding};
    margin-right: ${ThemeHorizontalPadding};

    color: ${ThemePrimaryTextColor};
    background: ${ThemeTertiaryBackgroundColor};
  }

  .input:last-child {
    margin-bottom: 0;
  }

  .input label {
    padding-top: ${ThemeVerticalHalfPadding};
    padding-bottom: ${ThemeVerticalHalfPadding};
    padding-left: ${ThemeHorizontalPadding};
    padding-right: ${ThemeHorizontalPadding};
    min-width: 75px;
    font-size: ${ThemeMediumTextSize};
    flex-grow: 0;
    text-align: center;
    border-radius: 5px 0 0 5px;
    background: ${ThemeQuaternaryBackgroundColor};
  }

  .input input,
  .input select {
    flex-grow: 1;
    border: none;
    margin: 0 0 0 10px;
    padding: 0;
    line-height: 1;
    outline: none;
    font-size: ${ThemeMediumTextSize};
    color: inherit;
    border-radius: 0 0 5px 5px;
    background: ${ThemeTertiaryBackgroundColor};
  }

  .input select {
    margin-right: 10px;
  }

  button.removeAvatar {
    display: block;
    margin-top: ${ThemeVerticalPadding};
    margin-bottom: ${ThemeVerticalPadding};
    color: ${ThemePrimaryTextColor};
    font-size: ${ThemeSmallTextSize};
    margin-left: auto;
    margin-right: auto;
  }

  p.msg {
    text-align: center;
    color: ${ThemePrimaryTextColor};
  }

  span {
    display: block;
    font-size: ${ThemeSmallTextSize};
    text-align: center;
    color: ${ThemeSecondaryTextColor};
  }
`;

const Avatar = styled.img`
  display: block;
  height: auto;
  margin: auto;
`;

const UserAvatar = ({ user }) => {
  const src = user.avatar_url;
  const alt = user.username;
  const date = new Date();
  let url = `${config.cdnHost}/image/${src}?t=${date.getTime()}`;
  if (!src || src.length === 0) {
    url = '/static/default-avatar.png';
  }
  return <Avatar src={url} alt={`${alt}'s Avatar`} />;
};
UserAvatar.propTypes = {
  user: PropTypes.shape({
    avatar_url: PropTypes.string.isRequired,
    username: PropTypes.string.isRequired,
  }).isRequired,
};

const UserBackground = ({ backgroundUrl, user }) => {
  const src = backgroundUrl;
  const alt = user.username;
  let url = `${config.cdnHost}/image/${src}`;
  if (!src || src.length === 0) {
    url = '/static/default-avatar.png';
  }
  return <Avatar src={url} alt={`${alt}'s Background`} />;
};
UserBackground.propTypes = {
  backgroundUrl: PropTypes.string.isRequired,
  user: PropTypes.shape({
    avatar_url: PropTypes.string.isRequired,
    username: PropTypes.string.isRequired,
  }),
};
UserBackground.defaultProps = {
  user: {
    avatar_url: undefined,
    username: undefined,
  },
};

class UserSettings extends React.Component {
  state = {
    uploaded: false,
    backgroundUploaded: false,
    color: loadThemeFromStorage(),
    width: loadWidthFromStorage(),
    scale: loadScaleFromStorage(),
    autoSubscribe: loadAutoSubscribeFromStorage(),
    ratingsXray: loadRatingsXrayFromStorage(),
    stickyHeader: loadStickyHeaderFromStorageBoolean(),
    punchyLabs: loadPunchyLabsFromStorageBoolean(),
    sendCountryInfo: loadDisplayCountrySettingFromStorage(),
    backgroundUrl: 'none.webp',
    showDeleteAccount: false,
    nsfwFilter: loadDisplayNsfwFilterSettingFromStorageBoolean(),
  };

  componentDidMount = async () => {
    const user = loadUserFromStorage();
    const date = new Date();

    const { ratingsHiddenForUser } = await getProfileRatingsDisplay();

    this.setState({
      backgroundUrl: `${user.background_url}?t=${date.getTime()}`,
      ratingsHiddenForUser,
    });
  };

  handleColorChange = (value) => {
    setThemeToStorage(value);
    this.setState({
      color: value,
    });
    window.location.reload();
  };

  handleWidthChange = (value) => {
    setWidthToStorage(value);
    this.setState({
      width: value,
    });
    window.location.reload();
  };

  handleScaleChange = (value) => {
    setScaleToStorage(value);
    this.setState({
      scale: value,
    });
    window.location.reload();
  };

  handleUpload = async (file) => {
    this.setState({ uploaded: false });
    try {
      const result = await uploadAvatar(file);
      if (result.message) {
        await updateUser({ avatar_url: result.message });
        return this.setState({ uploaded: true });
      }
      throw new Error('Unprocessable entity.');
    } catch (err) {
      this.setState({ uploaded: false });
      return this.showImageUploadAlert();
    }
  };

  handleBackgroundUpload = async (file) => {
    this.setState({
      backgroundUploaded: false,
      backgroundUrl: `none.webp`,
    });
    try {
      const result = await uploadBackground(file);
      if (result.message) {
        await updateUser({ background_url: result.message });
        const date = new Date();
        return this.setState({
          backgroundUploaded: true,
          backgroundUrl: `${result.message}?t=${date.getTime()}`,
        });
      }
      throw new Error('Unprocessable entity.');
    } catch (err) {
      this.setState({ backgroundUploaded: false });
      return this.showImageUploadAlert();
    }
  };

  handleAutoSubscribeChange = (value) => {
    setAutoSubscribeToStorage(value);
    this.setState({
      autoSubscribe: value,
    });
    window.location.reload();
  };

  handleRatingsXrayChange = (value) => {
    setRatingsXrayToStorage(value);
    this.setState({
      ratingsXray: value,
    });
    window.location.reload();
  };

  handleStickyHeaderChange = (value) => {
    setStickyHeaderToStorage(value);
    this.setState({
      stickyHeader: value,
    });
    window.location.reload();
  };

  handlePunchyLabsChange = (value) => {
    setPunchyLabsToStorage(value);
    this.setState({
      punchyLabs: value,
    });
    window.location.reload();
  };

  handleSendCountryInfoToggle = ({ target }) => {
    saveDisplayCountryInfoSettingToStorage(target.value === 'true');
    this.setState({ sendCountryInfo: target.value === 'true' });
  };

  handleNsfwFilterChange = ({ target }) => {
    saveDisplayNsfwFilterSettingToStorage(target.value === 'true');
    this.setState({ nsfwFilter: target.value === 'true' });
  };

  handleHideProfileRatingsChange = async (value) => {
    await updateProfileRatingsDisplay(value === 'true');
    this.setState({
      ratingsHiddenForUser: value === 'true',
    });
  };

  showImageUploadAlert = () =>
    pushSmartNotification({
      error: 'Upload failed. Are you uploading a JPG/GIF/PNG image? Is the image over 2MB?',
    });

  setEmptyAvatar = async () => {
    await updateUser({ avatar_url: 'none.webp' });
  };

  renderPatreonButton = () => (
    <LoginButton color="white" background="#e85b46" route="/link/patreon">
      <i className="fab fa-patreon" />
      <span style={{ color: 'white' }}>Claim Bonuses</span>
    </LoginButton>
  );

  renderAccountDeletionButton = () => (
    <StyledDeleteAccountButton
      background="#ad1a1a"
      onClick={() => {
        this.setState({ showDeleteAccount: true });
        scrollToTop();
      }}
      type="button"
    >
      <i className="fas fa-bomb" />
      Delete account
    </StyledDeleteAccountButton>
  );

  render() {
    const { currentUser } = this.props;
    const { showDeleteAccount } = this.state;
    return (
      <div>
        <LoggedInOnly>
          <SubHeader>
            <SubHeaderBackAndTitle>
              <SubHeaderBack to="/">&#8249;</SubHeaderBack>
              <SubHeaderTitle>Edit Profile</SubHeaderTitle>
            </SubHeaderBackAndTitle>
            <SubHeaderPaginationAndButtons>
              <SubHeaderPagination />
              <SubHeaderDropdown>
                <SubHeaderDropdownArrow className="fa fa-angle-down" />
                <SubHeaderDropdownList>
                  <SubHeaderDropdownListItem>
                    <SubHeaderButton to="/logout">
                      <i className="fas fa-sign-out-alt" />
                      <span>Log me out!</span>
                    </SubHeaderButton>
                  </SubHeaderDropdownListItem>
                </SubHeaderDropdownList>
              </SubHeaderDropdown>
            </SubHeaderPaginationAndButtons>
          </SubHeader>

          <PageWrapper>
            <div className="column">
              <Panel>
                <h2>Avatar Upload</h2>
                <label className="avatar fg" htmlFor="upload">
                  <UserAvatar user={currentUser} fresh="true" />
                  <i className="fas fa-file-upload" />
                  <input
                    id="upload"
                    type="file"
                    name="avatar"
                    accept="image/*"
                    onChange={(e) => this.handleUpload(e.target.files[0])}
                  />
                </label>
                {this.state.uploaded && (
                  <>
                    <p className="msg">
                      Your avatar has been updated successfully! It might take up to 120s to update.
                    </p>
                  </>
                )}
                <span>
                  Select a file to upload
                  <br />
                  (115x115, resizing before upload recommended)
                  <UserGroupRestricted userGroupIds={[2]}>
                    <br />
                    You must be a Gold member to upload animated avatars.
                  </UserGroupRestricted>
                  <UserGroupRestricted userGroupIds={[2]}>
                    <br />
                    Animated avatars: 115x115 <b>MAX</b>, 125kb <b>MAX</b>, GIFs <b>ONLY</b>.
                    Anything that doesn&apos;t match that will be un-animated.
                  </UserGroupRestricted>
                </span>

                <DefaultBlueHollowButton className="removeAvatar" onClick={this.setEmptyAvatar}>
                  Remove
                </DefaultBlueHollowButton>
              </Panel>

              <Panel>
                <h2>Theme</h2>
                <div className="input">
                  <label>Colour</label>
                  <select
                    onChange={(e) => this.handleColorChange(e.target.value)}
                    value={this.state.color}
                  >
                    <option value="dark">Dark</option>
                    <option value="light">Light</option>
                  </select>
                </div>
                <div className="input">
                  <label>Width</label>
                  <select
                    onChange={(e) => this.handleWidthChange(e.target.value)}
                    value={this.state.width}
                  >
                    <option value="full">Full</option>
                    <option value="verywide">Very Wide</option>
                    <option value="wide">Wide</option>
                    <option value="medium">Medium</option>
                    <option value="narrow">Narrow</option>
                  </select>
                </div>
                <div className="input">
                  <label>Scale</label>
                  <select
                    onChange={(e) => this.handleScaleChange(e.target.value)}
                    value={this.state.scale}
                  >
                    <option value="large">Thicc</option>
                    <option value="medium">Standard</option>
                    <option value="small">Sticc</option>
                  </select>
                </div>
              </Panel>

              <Panel>
                <h2 title="Because you're important to us <3">User Experience</h2>

                <div className="input">
                  <Tooltip text="Subscribe to threads on reply / on creation">
                    <label>AutoSub™</label>
                  </Tooltip>
                  <select
                    onChange={(e) => this.handleAutoSubscribeChange(e.target.value)}
                    value={this.state.autoSubscribe}
                  >
                    <option value="true">Enabled</option>
                    <option value="false">Disabled</option>
                  </select>
                </div>

                <div className="input">
                  <Tooltip text="See who left that *dumb* rating">
                    <label>Ratings X-ray™</label>
                  </Tooltip>
                  <select
                    onChange={(e) => this.handleRatingsXrayChange(e.target.value)}
                    value={this.state.ratingsXray}
                  >
                    <option value="true">Enabled</option>
                    <option value="false">Disabled</option>
                  </select>
                </div>

                <div className="input">
                  <Tooltip text="Sticky header that's always with you as you scroll">
                    <label>StickyHeader™</label>
                  </Tooltip>
                  <select
                    onChange={(e) => this.handleStickyHeaderChange(e.target.value)}
                    value={this.state.stickyHeader}
                  >
                    <option value="true">Enabled</option>
                    <option value="false">Disabled</option>
                  </select>
                </div>

                <div className="input">
                  <Tooltip text="Hide ratings on your profile">
                    <label>BoxHide™</label>
                  </Tooltip>
                  <select
                    onChange={(e) => this.handleHideProfileRatingsChange(e.target.value)}
                    value={this.state.ratingsHiddenForUser}
                  >
                    <option value="true">Enabled</option>
                    <option value="false">Disabled</option>
                  </select>
                </div>

                <div className="input">
                  <Tooltip text="Show country on posts">
                    <label>FlagPunchy™</label>
                  </Tooltip>
                  <select
                    onChange={(e) => this.handleSendCountryInfoToggle(e)}
                    value={this.state.sendCountryInfo}
                  >
                    <option value="true">Enabled</option>
                    <option value="false">Disabled</option>
                  </select>
                </div>

                <div className="input">
                  <Tooltip text="Experimental and buggy settings">
                    <label>PunchyLabs™</label>
                  </Tooltip>
                  <select
                    onChange={(e) => this.handlePunchyLabsChange(e.target.value)}
                    value={this.state.punchyLabs}
                  >
                    <option value="true">Enabled</option>
                    <option value="false">Disabled</option>
                  </select>
                </div>

                <div className="input">
                  <Tooltip text="Hides NSFW threads">
                    <label>WorkSafe™</label>
                  </Tooltip>
                  <select
                    onChange={(e) => this.handleNsfwFilterChange(e)}
                    value={this.state.nsfwFilter}
                  >
                    <option value="true">Enabled</option>
                    <option value="false">Disabled</option>
                  </select>
                </div>
              </Panel>

              <Panel>
                <h2 title="Richboi Zone">
                  Patreon Bonuses (connects accounts permanently - don't use if you're not a Patreon
                  supporter)
                </h2>
                {this.renderPatreonButton()}
                <div />
              </Panel>

              <Panel>
                <h2 title="Delete Account">Delete Account</h2>
                {this.renderAccountDeletionButton()}
                <div />
              </Panel>
            </div>

            <div className="column">
              <Panel>
                <h2>Background Upload</h2>
                <label className="avatar bg" htmlFor="bgupload">
                  <UserBackground backgroundUrl={this.state.backgroundUrl} user={currentUser} />
                  <i className="fas fa-file-upload" />
                  <input
                    id="bgupload"
                    type="file"
                    name="background"
                    accept="image/*"
                    onChange={(e) => this.handleBackgroundUpload(e.target.files[0])}
                  />
                </label>
                {this.state.backgroundUploaded && (
                  <p className="msg">
                    Your background has been updated successfully! It might take up to 120s to
                    update.
                  </p>
                )}
                <span>
                  Select a file to upload
                  <br />
                  (230x460, resizing before upload recommended)
                </span>
              </Panel>
            </div>
          </PageWrapper>

          {showDeleteAccount && <DeleteAccountWarning />}
        </LoggedInOnly>
      </div>
    );
  }
}
UserSettings.propTypes = {
  currentUser: PropTypes.shape({
    avatar_url: PropTypes.string.isRequired,
    username: PropTypes.string.isRequired,
  }).isRequired,
};

const mapStateToProps = ({ user }) => ({
  currentUser: user,
});
export default connect(mapStateToProps)(UserSettings);
