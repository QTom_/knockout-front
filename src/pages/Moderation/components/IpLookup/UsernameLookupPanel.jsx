import React from 'react';
import {
  Panel,
  PanelHeader,
  PanelSearchField,
  PanelSearchButton,
  PanelBody,
  PanelBodyPlaceholder
} from './style';
import { getIpsByUsername } from '../../../../services/moderation';

const Results = ({ results }) => (
  <tbody>
    {results.map(item => (
      <tr key={item}>
        <td>{item.id}</td>
        <td>{item.username}</td>
        <td>
          {item.ipAddresses.map(ip => (
            <span key={ip}>{ip}</span>
          ))}
        </td>
      </tr>
    ))}
  </tbody>
);

class UsernameLookupPanel extends React.Component {
  state = {
    username: '',
    results: [],
    searched: false
  };

  setUsername = e => this.setState({ username: e.target.value });

  search = async () => {
    const result = await getIpsByUsername(this.state.username);
    this.setState({
      results: result,
      searched: true
    });
  };

  render() {
    return (
      <Panel>
        <PanelHeader>
          <PanelSearchField
            type="text"
            value={this.state.username}
            onChange={this.setUsername}
            placeholder="Search IP by user"
          />
          <PanelSearchButton onClick={this.search}>Search</PanelSearchButton>
        </PanelHeader>
        {this.state.searched && (
          <PanelBody>
            <table>
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Username</th>
                  <th>Addresses</th>
                </tr>
              </thead>
              <Results results={this.state.results} />
            </table>
          </PanelBody>
        )}
        {!this.state.searched && (
          <PanelBody>
            <PanelBodyPlaceholder>Please enter a search term above</PanelBodyPlaceholder>
          </PanelBody>
        )}
      </Panel>
    );
  }
}

export default UsernameLookupPanel;
