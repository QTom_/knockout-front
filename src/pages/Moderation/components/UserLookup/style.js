import styled from 'styled-components';
import {
  ThemeHorizontalPadding,
  ThemeHorizontalHalfPadding,
  ThemeVerticalPadding,
  ThemeVerticalHalfPadding,
  ThemeSecondaryBackgroundColor,
  ThemeTertiaryBackgroundColor,
  ThemePrimaryTextColor,
  ThemeSecondaryTextColor
} from '../../../../Theme';
import { MobileMediaQuery } from '../../../../components/SharedStyles';

export const Panels = styled.div`
  display: flex;
  flex-direction: row;
  max-width: 1280px;
  margin: 0 auto;

  ${MobileMediaQuery} {
    flex-direction: column;
  }
`;

export const Panel = styled.div`
  position: relative;
  background-color: ${ThemeSecondaryBackgroundColor};
  min-width: 50%;
  width: 100%;
  margin: ${ThemeVerticalHalfPadding} ${ThemeHorizontalHalfPadding};
  border-radius: 5px;

  ${MobileMediaQuery} {
    width: auto;
    flex-basis: 2;
  }
`;

export const PanelHeader = styled.div`
  position: relative;
  display: flex;
  flex-direction: row;
  border-radius: 5px 5px 0 0;
  background-color: ${ThemeTertiaryBackgroundColor};

  ${props =>
    props.centerWide &&
    `
    width: 100%;
    max-width: 800px;
    margin: 0 auto;
  `}
`;

export const PanelSearchField = styled.input`
  flex-grow: 1;
  background: transparent;
  border: none;
  outline: none;
  line-height: 22px;
  color: ${ThemePrimaryTextColor};
  padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
`;

export const PanelSearchButton = styled.button`
  background: transparent;
  border: none;
  outline: none;
  line-height: 22px;
  cursor: pointer;
  color: ${ThemePrimaryTextColor};
  padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
`;

export const PanelBody = styled.div`
  position: relative;
  min-height: 222px;
  padding: ${ThemeVerticalHalfPadding} ${ThemeHorizontalPadding};

  table {
    width: 100%;
  }

  table th {
    color: ${ThemeSecondaryTextColor};
    padding: ${ThemeVerticalHalfPadding} 0;
    text-align: left;
  }

  table td {
    padding: ${ThemeVerticalHalfPadding} 0;
  }

  table td span {
    display: inline-block;
    border-radius: 50px;
    padding: ${ThemeVerticalHalfPadding} ${ThemeHorizontalPadding};
    margin-right: ${ThemeHorizontalHalfPadding};
    margin-bottom: ${ThemeVerticalHalfPadding};
    color: ${ThemePrimaryTextColor};
    background-color: ${ThemeTertiaryBackgroundColor};
  }
`;

export const PanelBodyPlaceholder = styled.div`
  text-align: center;
  padding: 100px 0;
  line-height: 22px;
  color: ${ThemeSecondaryTextColor};
`;
