/* eslint-disable */
import React from 'react';
import { Wrapper, List, ListItem, ListItemLink } from './style';

const NavbarResponsive = ({ items }) => (
  <Wrapper>
    <List>
      {items.map((item, index) => (
        <ListItem key={item.url}>
          <ListItemLink to={item.url}>
            <i className={item.icon} />
            <span>{item.title}</span>
          </ListItemLink>
        </ListItem>
      ))}
    </List>
  </Wrapper>
);

export default NavbarResponsive;
