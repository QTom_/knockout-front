import React from 'react';
import styled, { keyframes } from 'styled-components';
import PropTypes from 'prop-types';
import LastPostInfo from './LastPostInfo';
import {
  ThemeVerticalPadding,
  ThemeHorizontalPadding,
  ThemeBackgroundLighter,
  ThemeBackgroundDarker
} from '../../../Theme';

const SubforumInfo = ({ subforum }) => {
  if (subforum.lastPost) {
    return (
      <StyledSummaryPlaceholder>
        <LastPostInfo post={subforum.lastPost} withTitle />
      </StyledSummaryPlaceholder>
    );
  }
  return (
    <StyledSummaryPlaceholder className="sfitem-description">
      <div className="fake-title" />
      <div className="fake-postinfo" />
      <div className="fake-gotopage" />
    </StyledSummaryPlaceholder>
  );
};

SubforumInfo.propTypes = {
  subforum: PropTypes.shape({
    lastPost: PropTypes.shape({
      user: PropTypes.shape({
        username: PropTypes.string.isRequired,
        usergroup: PropTypes.number.isRequired
      }).isRequired
    }),
    name: PropTypes.string.isRequired,
    totalThreads: PropTypes.number.isRequired,
    totalPosts: PropTypes.number.isRequired
  }).isRequired
};

export default SubforumInfo;

const placeholderAnim = keyframes`
  0% {
    transform: translateX(0%);
  }
  100% {
    transform: translateX(-75%);
  }
`;

const StyledSummaryPlaceholder = styled.summary`
  width: 100%;
  position: relative;

  .fake-title,
  .fake-postinfo,
  .fake-gotopage {
    position: absolute;
    height: 12px;
    border-radius: 4px;
    overflow: hidden;

    &:after {
      content: '';
      display: block;
      width: 400%;
      height: 100%;
      left: 0;
      position: absolute;
      background: linear-gradient(
        90deg,
        ${ThemeBackgroundLighter} 0%,
        ${ThemeBackgroundDarker} 25%,
        ${ThemeBackgroundLighter} 75%,
        ${ThemeBackgroundDarker} 100%
      );
      animation: ${placeholderAnim} 1s linear infinite;
    }
  }
  .fake-title {
    width: 100px;
    top: calc(${ThemeVerticalPadding} + 4px);
    left: ${ThemeHorizontalPadding};
  }
  .fake-postinfo {
    width: 220px;
    bottom: calc(${ThemeVerticalPadding} + 4px);
    left: ${ThemeHorizontalPadding};
  }
  .fake-gotopage {
    width: 50px;
    bottom: calc(${ThemeVerticalPadding} + 4px);
    right: calc(${ThemeHorizontalPadding} + 25px);
  }
`;
