import React from 'react';
import styled, { keyframes } from 'styled-components';
import { Helmet } from 'react-helmet';

const LainBackground = styled.article`
  position: fixed;
  z-index: 99996;
  /* lets all love lain. */
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  background: url('https://fauux.neocities.org/bg_46.gif');
  /* Let's all love Lain. */
  cursor: url('https://fauux.neocities.org/cursor.gif'), auto;

  font-size: x-small;
  color: rgb(193, 180, 146);
`;

const LainBackgroundAlt = styled.div`
  opacity: 0;
  pointer-events: none;
  position: fixed;
  z-index: 99997;
  /* Let's all love Lain. Let's all love Lain. Let's all love Lain.  */
  top: 0;
  bottom: 0;
  left: 0;
  mix-blend-mode: color;
  right: 0;
  background: url('https://fauux.neocities.org/Static2.gif');
  /*  Let's all love Lain. Let's all love Lain. Let's all love Lain.  Let's all love Lain. Let's all love Lain. Let's all love Lain.  Let's all love Lain. Let's all love Lain. Let's all love Lain.  */
`;

const LainImageAlt = styled.img`
  opacity: 0;
  pointer-events: none;
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  width: 100vh;
  z-index: 99998;
  mix-blend-mode: lighten;
`;

const LainImage = styled.img`
  position: absolute;
  top: 50%;
  /* Lain Lain Lain Lain Lain Lain Lain LainLainLainLainLainLainLainLainLainLainLain */
  left: 50%;
  /* i love you lain */
  transform: translate(-50%, -50%);

  &:hover {
    ~ ${LainBackgroundAlt} {
      opacity: 1;
    }
    ~ ${LainImageAlt} {
      opacity: 1;
    }
  }
`;

const LainLovesMeToo = styled.div`
  position: absolute;
  top: ${Math.random() * 80}%;
  left: ${Math.random() * 80}%;
  max-width: ${Math.random * 50 + 280}px;
  font-size: 14px;
  font-family: monospace;
`;

const blinkanim = keyframes`
  0% {
    opacity: 0.1;
  } 50% {
    opacity: 1;
  } 75% {
    opacity: 0.4;
  } 100% {
    opacity: 0.8;
  }
`;

const AndILoveLain = styled.span`
  animation: ${blinkanim} 150ms alternate infinite;
`;

class LoveHer extends React.Component {
  YoutubeRef = React.createRef();

  render() {
    return (
      <LainBackground>
        <Helmet>
          <title>ï½’ï½ï½‰ï½Ž</title>
        </Helmet>
        <audio autoPlay="0" loop>
          <source src="https://fauux.neocities.org/sound/boon_loop.wav" type="audio/wav" />
        </audio>
        <LainImage
          onMouseEnter={() => {
            this.YoutubeRef.current.contentWindow.postMessage(
              '{"event":"command","func":"playVideo","args":""}',
              '*'
            );
          }}
          onMouseLeave={() => {
            this.YoutubeRef.current.contentWindow.postMessage(
              '{"event":"command","func":"pauseVideo","args":""}',
              '*'
            );
          }}
          src="https://fauux.neocities.org/15c.gif"
          alt="love lain."
        />
        <LainImageAlt src="https://fauux.neocities.org/lainbighead3.gif" alt="love lain." />
        <LainBackgroundAlt />
        <iframe
          title="lets all love lain."
          width="0"
          height="0"
          src="//www.youtube.com/embed/b4fl85vEm0s?loop=1&playlist=ltSySjNZCtk&enablejsapi=1&version=3&playerapiid=ytplayer"
          frameBorder="0"
          allowFullScreen=""
          ref={this.YoutubeRef}
        />

        <LainLovesMeToo>
          ã€Œã€€ï¼©ã€€ï½“ï½ˆï½ï½Œï½Œã€€ï½„ï½’ï½ï½‰ï½Žã€€ï½™ï½ï½•ï½’ã€€ï½ï½‰ï½Žï½„ï½“
          ï½ï½Žï½„ã€€ï½‚ï½…ï½Žï½„ã€€ï½™ï½ï½•ï½’ã€€ï½—ï½‰ï½Œï½Œã€€ï½”ï½ã€€ï½ï½‰ï½Žï½…ï¼Ž
          ï¼¹ï½ï½•ã€€ï½—ï½‰ï½Œï½Œã€€ï½“
          <AndILoveLain>ï½•ï½‚ï½ï½‰ï½”ï¼Œ</AndILoveLain>
          ï½ï½Žï½„ã€€ï¼©ã€€ï½“ï½ˆï½ï½Œï½Œã€€ï½ï½ï½“ï½“ï½…ï½“ï½“ã€€ï½™ï½ï½•ï¼Žã€
          ï¼ï½Žï½…ï½ï½Œï½ï½‰ï½Ž
        </LainLovesMeToo>
      </LainBackground>
    );
  }
}

export default LoveHer;
