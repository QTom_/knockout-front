import React from 'react';
import styled from 'styled-components';

import { TitleLarge, InformationalTextBody } from '../../../components/SharedStyles';

const PrivacyPolicy = () => (
  <>
    <TitleLarge>Privacy Policy and Terms of Use</TitleLarge>

    <InformationalTextBody>
      If you want to use Knockout, you must read and agree to all of the below. It’s important, so
      read all of it!
    </InformationalTextBody>

    <InformationalTextBody>
      For us to function right now, we do have to (unfortunately) collect some data. In the future
      we&apos;d prefer not to save any of this data, or to obfuscate it so that in the case
      something bad happens, it makes attackers&apos; lives harder.
    </InformationalTextBody>

    <InformationalTextBody>Data we collect and store:</InformationalTextBody>

    <PrivacyPolicyList>
      <li>Username</li>
      <li>The Account IT associated with your login provider (Google, Twitter, etc)</li>
      <li>The IP(s) you were using when creating a new post or thread</li>
    </PrivacyPolicyList>

    <InformationalTextBody>Data we do not collect:</InformationalTextBody>

    <PrivacyPolicyList>
      <li>Email Address</li>
      <li>Real name</li>
      <li>Fine location</li>
      <li>
        Age - that said, you <b>must</b> be 18 or older to use this site.
      </li>
      <li>Sex</li>
    </PrivacyPolicyList>

    <InformationalTextBody>Data Retention</InformationalTextBody>

    <p>
      Hourly backups of the production databases are taken, these backups are retained for a month
      before they “rotate out” and are disposed of. Any accounts deleted will remain in the backups
      until the files expire after a month.
    </p>
    <p>
      Web server access and error logs are retained for a month before being disposed of, these logs
      contain your IP Addresses, which pages you have visited and information about your web browser
      version. These logs exist for diagnostic purposes and for investigating malicious traffic.
    </p>
    <p>
      IP Addresses are retained indefinitely to allow us to identify abusive users and implement IP
      bans for said users.
    </p>

    <InformationalTextBody>Data Processing</InformationalTextBody>
    <p>
      Your IP Address may be crosschecked against several databases to identify traffic sources and
      rough geographical location. You may opt out of geoip processing within your account settings,
      this is an optional feature to enable public country information on your posts.
    </p>

    <InformationalTextBody>Data Privacy</InformationalTextBody>
    <p>
      The limited personal information we store will never be shared with any third party (except
      law enforcement organisations upon request), and any processing of your personal information
      is handled internally with no dependencies on third parties.
    </p>

    <TitleLarge>Right to Delete</TitleLarge>

    <InformationalTextBody>
      We want to be as privacy-friendly as possible. Do your part:&nbsp;
      <b>do not post any personally-identifiable info in this site.</b> When you ask an admin for
      your account to be deleted, some data will be retained in our database in order for us to keep
      offering a quality service:
    </InformationalTextBody>

    <PrivacyPolicyList>
      <li>Your username will be modified to a default deleted user username,</li>
      <li>
        Your login provider ID <b>will </b>
        be retained, privately, so that you can&apos;t create another user in order to evade bans
      </li>
      <li>
        Your IPs used for posting <b>will </b>
        be retained, for the same reasons as above. Alts are not allowed.
      </li>
    </PrivacyPolicyList>

    <TitleLarge>DO NOT POST PERSONALLY IDENTIFIABLE INFO IN THIS SITE</TitleLarge>

    <InformationalTextBody>
      Do not post personally identifiable info on this site. If you delete your user the posts you
      submitted to this site will be retained.
    </InformationalTextBody>

    <TitleLarge>Follow The Rules Or Get Banned</TitleLarge>

    <InformationalTextBody>
      If you&apos;re new to this site, know that sometimes you&apos;ll get banned as a warning. If
      you want to minimize the chances that this will happen to you, read the rules and pay
      attention to the etiquette of other users.
    </InformationalTextBody>

    <TitleLarge>HOLY HECK DO NOT POST PERSONALLY IDENTIFIABLE INFO IN THIS SITE</TitleLarge>

    <InformationalTextBody>
      Do not post personally identifiable info on this site. Do not post personally identifiable
      info on this site. Do not post personally identifiable info on this site. Do not post
      personally identifiable info on this site. If you delete your user the posts you submitted to
      this site will be retained.
    </InformationalTextBody>

    <InformationalTextBody>
      If you&apos;ve read all of the above and are okay with all of it, set up your account below.
      If not, it&apos;s time to close this tab and to never return.
    </InformationalTextBody>
  </>
);

export default PrivacyPolicy;

const PrivacyPolicyList = styled.ul`
  padding: 0;
  margin: 0 20px;
  line-height: 1.5;
`;
