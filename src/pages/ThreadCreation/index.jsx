import React from 'react';
import PropTypes from 'prop-types';

import { listIcons } from '../../services/icons';

import { usergroupCheck } from '../../components/UserGroupRestricted';
import ThreadCreationComponent from './ThreadCreationComponent';
import { getTags } from '../../services/tags';
import { MODERATOR_GROUPS } from '../../utils/userGroups';

class ThreadCreation extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      title: '',
      content: '',
      icon_id: 0,
      icons: null,
      backgroundType: 'cover',
      backgroundUrl: null,
      tags: [],
      selectedTags: [],
      currentTag: 'default'
    };
  }

  async componentDidMount() {
    this.loadIcons();
    const tags = await getTags();

    this.setState({ tags });
  }

  formIsValid = () =>
    this.state.title.length < 80 &&
    this.state.title.length > 3 &&
    this.state.content.length > 0 &&
    this.state.icon_id !== 0;

  handleTitleChange = ({ target }) => {
    this.setState({
      [target.name]: target.value
    });
  };

  handleContentChange = text => {
    this.setState({ content: text });
  };

  handleIconChange = icon => {
    this.setState({ icon_id: icon.id });
  };

  handleBackgroundChange = ({ target }) => {
    this.setState({ backgroundUrl: target.value });
  };

  handleBackgroundTypeChange = ({ target }) => {
    this.setState({ backgroundType: target.value });
  };

  loadIcons = async () => {
    const userHasAdminThreadIconPerms = usergroupCheck(MODERATOR_GROUPS);

    const icons = await listIcons({
      showAdminIcons: userHasAdminThreadIconPerms
    });
    this.setState({ icons });
  };

  handleTagAdd = e => {
    if (this.state.selectedTags.length === 3) {
      return;
    }

    const { value } = e.target;

    this.setState(prevState => ({
      selectedTags: [...prevState.selectedTags, value],
      currentTag: 'default'
    }));
  };

  handleTagRemove = index => {
    const newTags = [...this.state.selectedTags];
    newTags.splice(index, 1);

    this.setState({
      selectedTags: newTags
    });
  };

  getTagName = str => str.split('|')[1];

  render() {
    const {
      title,
      content,
      icon_id: iconId,
      icons,
      backgroundType,
      backgroundUrl,
      tags,
      selectedTags,
      currentTag
    } = this.state;

    return (
      <ThreadCreationComponent
        threadTitle={title}
        icons={icons}
        backgroundType={backgroundType}
        backgroundUrl={backgroundUrl}
        iconId={iconId}
        content={content}
        getEditor={this.getEditor}
        refreshPosts={this.refreshPosts}
        handleTitleChange={this.handleTitleChange}
        handleBackgroundChange={this.handleBackgroundChange}
        handleBackgroundTypeChange={this.handleBackgroundTypeChange}
        handleIconChange={this.handleIconChange}
        tags={tags}
        handleTagAdd={this.handleTagAdd}
        selectedTags={selectedTags}
        currentTag={currentTag}
        getTagName={this.getTagName}
        handleTagRemove={this.handleTagRemove}
      />
    );
  }
}
ThreadCreation.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string.isRequired
    }).isRequired
  }).isRequired
};

export default ThreadCreation;
