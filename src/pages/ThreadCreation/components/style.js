import styled from 'styled-components';

import {
  ThemeVerticalPadding,
  ThemeHorizontalPadding,
  ThemeMediumTextSize,
  ThemeSecondaryBackgroundColor,
  ThemeTertiaryBackgroundColor,
  ThemeQuaternaryBackgroundColor,
  ThemePrimaryTextColor,
  ThemeKnockoutRed
} from '../../../Theme';

export const StyledThreadCreation = styled.article`
  margin: ${ThemeVerticalPadding} auto 0 auto;
  display: block;
  padding: 0 ${ThemeHorizontalPadding};

  .line-option-wrapper {
    display: flex;
    .input {
      flex: 1;

      &:first-child {
        margin-right: 5px;
      }
    }
    .input.tall {
      flex: 1;
      flex-shrink: 1;
    }

    @media (max-width: 960px) {
      display: initial;

      .input {
        &:first-child {
          margin-right: unset;
        }
      }
    }
  }

  .tooltip-content {
    display: flex;
  }

  .input {
    position: relative;
    display: flex;
    flex-direction: row;
    min-height: 32px;
    line-height: 32px;
    margin-bottom: 10px;
    color: ${ThemePrimaryTextColor};
    background: ${ThemeTertiaryBackgroundColor};
    border-radius: 5px;
  }

  .input.tall {
    @media (max-width: 700px) {
      flex-direction: column;
    }
  }

  .input .inputLabel {
    padding: 0 10px;
    min-width: 75px;
    font-size: 12px;
    flex-grow: 0;
    text-align: center;
    border-radius: 5px 0 0 5px;
    font-size: ${ThemeMediumTextSize};
    background: ${ThemeQuaternaryBackgroundColor};
  }

  .input.tall label {
    @media (max-width: 700px) {
      border-radius: 5px 5px 0 0;
    }
  }

  .input input {
    flex-grow: 1;
    border: none;
    margin: 0 0 0 ${ThemeHorizontalPadding};
    padding: 0;
    height: 32px;
    line-height: 32px;
    outline: none;
    font-size: ${ThemeMediumTextSize};
    background: transparent;
    color: ${ThemePrimaryTextColor};
  }

  .editor {
    background: ${ThemeSecondaryBackgroundColor};
    border-radius: 5px;
  }

  .selected-tags .tag-item {
    padding: 2px 6px;
    margin: 0 5px;
    background: ${ThemeKnockoutRed};
    border: none;
    border-radius: 10px;
    color: white;
    cursor: pointer;

    &:hover {
      filter: brightness(1.25);
    }
  }

  .tags-list {
    select {
      width: 100%;
      border: none;
      margin: 0 0 0 10px;
      padding: 0;
      line-height: 1;
      outline: none;
      font-size: ${ThemeMediumTextSize};
      color: inherit;
      border-radius: 0 0 5px 5px;
      background: ${ThemeTertiaryBackgroundColor};

      @media (max-width: 960px) {
        margin: 0;
        min-height: 32px;
        padding-left: 5px;
      }
    }
  }
`;

export const BackgroundSizeOptionsWrapper = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  font-size: 16px;
`;

export const StyledBgSizeLabel = styled.label`
  margin-left: 15px;
  background: none;
  font-size: 18px;
  border-radius: unset;

  ${props =>
    props.active &&
    ` color: #f44336;
    `}
`;
