import { withRouter } from 'react-router-dom';

import { createAlertRequest } from '../../../services/alerts';
import { pushSmartNotification } from '../../../utils/notification';
import { loadAutoSubscribeFromStorageBoolean } from '../../../services/theme';
import { usergroupCheck } from '../../../components/UserGroupRestricted';
import PostEditorBB from '../../../components/KnockoutBB/PostEditorBB';
import { loadUserFromStorage } from '../../../services/user';
import { GOLD_MEMBER_GROUPS } from '../../../utils/userGroups';

const MIN_ACCT_AGE_SUBFORUM_IDS = [5, 6]; // subforumIds where acct has to be at least 3 days old to post

class ThreadCreationEditorBB extends PostEditorBB {
  submitPost = async () => {
    this.setState({ canSubmit: false });

    const {
      threadTitle,
      iconId,
      createNewThread,
      backgroundUrl,
      backgroundType,
      selectedTags,
    } = this.props;

    try {
      if (!threadTitle || threadTitle.length < 4 || threadTitle.length > 140) {
        pushSmartNotification({ error: 'Bad title' });
        throw new Error({ error: 'Bad title' });
      }
      if (!iconId) {
        pushSmartNotification({ error: 'Choose an icon first!' });
        throw new Error({ error: 'Choose an icon first! ' });
      }

      const now = new Date();
      const cutoff = new Date(loadUserFromStorage().createdAt);
      cutoff.setDate(cutoff.getDate() + 7);

      const accountIsWeekOld = now.getTime() >= cutoff.getTime();

      if (!accountIsWeekOld && MIN_ACCT_AGE_SUBFORUM_IDS.includes(this.props.subforumId)) {
        return pushSmartNotification({
          error: `Your account is too new to post in this subforum. (ERR2NU-LRKMOAR)`,
        });
      }

      const userHasBackgroundPermissions = usergroupCheck(GOLD_MEMBER_GROUPS);

      const backgroundOptions = userHasBackgroundPermissions
        ? { backgroundUrl, backgroundType }
        : {};

      const getTagId = (str) => str.split('|')[0];
      const tags = selectedTags.map((tagString) => getTagId(tagString));

      const thread = await createNewThread({
        title: threadTitle,
        icon_id: iconId,
        subforum_id: this.props.match.params.id,
        content: this.state.content,
        tagIds: tags,
        ...backgroundOptions,
      });

      if (!thread || !thread.id) {
        pushSmartNotification({ error: 'Thread creation failed' });
        throw new Error('Could not create thread');
      }

      if (loadAutoSubscribeFromStorageBoolean()) {
        await createAlertRequest({
          threadId: thread.id,
          lastSeen: new Date(),
          previousLastSeen: new Date('01/01/1980'),
        });
      }

      this.props.history.push(`/thread/${thread.id}`);
    } catch (err) {
      this.setState({ canSubmit: true });
      console.log('Error creating thread:', err);
      console.error(err);
    }
    return undefined;
  };
}

export default withRouter(ThreadCreationEditorBB);
