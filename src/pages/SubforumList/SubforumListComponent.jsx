/* eslint-disable react/forbid-prop-types */
import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { Link } from 'react-router-dom';

import {
  SubforumDescription,
  SubforumItem,
  SubforumItemHeader,
  SubforumItemWrapper,
  SubforumListWrapper,
  SubforumTitle
} from './components/style';
import PopularThreads from '../../components/PopularThreads';
import SubforumInfo from './components/SubforumInfo';

const SubforumListComponent = ({ subforums, defaultColor, defaultIcon }) => (
  <SubforumListWrapper>
    <Helmet>
      <title>Knockout!</title>
    </Helmet>

    <SubforumItemWrapper>
      {subforums.map((subforum, index) => (
        <SubforumItem key={subforum.id + subforum.name} id={`subforum-${subforum.id}`}>
          <Link
            className="subforum-item-header"
            to={`/subforum/${subforum.id}`}
            color={defaultColor(index)}
            background-image={subforum.icon || defaultIcon}
          >
            <SubforumTitle>{subforum.name}</SubforumTitle>
            <SubforumDescription>{subforum.description}</SubforumDescription>
          </Link>
          <SubforumInfo color={defaultColor(index)} subforum={subforum} />
        </SubforumItem>
      ))}
    </SubforumItemWrapper>

    <PopularThreads />
  </SubforumListWrapper>
);

SubforumListComponent.propTypes = {
  subforums: PropTypes.array.isRequired,
  defaultColor: PropTypes.func.isRequired,
  defaultIcon: PropTypes.string.isRequired
};

export default SubforumListComponent;
