import React from 'react';


export const wew = ({ payload }) => {
  if (!payload[0]) return null;

  return <div>{payload[0].payload.name}</div>;
};

export const CustomizedAxisTick = props => {
  const { x, y, stroke, payload } = props;

  return (
    <g transform={`translate(${x},${y})`}>
      <text x={0} y={0} dy={16} textAnchor="end" fill="#666" transform="rotate(-35)">
        {payload.value}
      </text>
    </g>
  );
};