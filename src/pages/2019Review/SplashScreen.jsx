/* eslint-disable react/button-has-type */
import React from 'react';
import { StyledYearReviewWrapper } from './styles';


const SplashScreen = ({ onClick }) => (
  <StyledYearReviewWrapper style={{ textAlign: 'center' }}>
    <img className="nerv-logo" src="static/other/2019review/nerv.svg" alt="" />
    <button className="begin-btn" onClick={onClick}>
      BEGIN
    </button>
    <p className="sound-warning">
      SOUND <s>ONLY</s> ALSO
    </p>
  </StyledYearReviewWrapper>
);

export default SplashScreen;
