import styled from 'styled-components';
import { ThemeVerticalPadding, ThemeHorizontalPadding } from '../../../Theme';

export const PageWrapper = styled.div`
  margin: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
`;
export const NoReports = styled.div`
  text-align: center;
  font-size: 20px;
  padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
`;
