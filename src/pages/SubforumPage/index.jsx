import React from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';

import { getSubforumWithThreads } from '../../services/subforums';
import SubforumPageComponent from './SubforumPage';
import { loadDisplayNsfwFilterSettingFromStorageBoolean } from '../../utils/postOptionsStorage';
import { filterNsfwThreads } from '../../utils/filterNsfwThreads';

class SubforumPage extends React.Component {
  placeholderThread = {
    icon_id: 0,
    id: 1,
    placeholder: true,
  };

  placeholderThreadArray = [
    { ...this.placeholderThread, id: 1 },
    { ...this.placeholderThread, id: 2 },
    { ...this.placeholderThread, id: 3 },
    { ...this.placeholderThread, id: 4 },
    { ...this.placeholderThread, id: 5 },
  ];

  constructor(props) {
    super(props);

    this.state = {
      subforum: {
        icon_id: 0,
        id: 0,
        name: 'Loading subforum...',
        threads: this.placeholderThreadArray,
        totalThreads: 0,
      },
      threadsHidden: false,
    };
  }

  async componentDidMount() {
    const data = await getSubforumWithThreads(
      this.props.match.params.id,
      this.props.match.params.page
    );

    const nsfwFilterEnabled = loadDisplayNsfwFilterSettingFromStorageBoolean();

    if (nsfwFilterEnabled) {
      const filteredThreads = filterNsfwThreads(data.threads);

      return this.setState({
        subforum: { ...data, threads: filteredThreads },
        threadsHidden: true,
      });
    }

    return this.setState({ subforum: data, threadsHidden: false });
  }

  /**
  * Warning: This lifecycle is currently deprecated, and will be removed in React version 17+
  More details here: https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html
  */
  async componentWillReceiveProps(nextProps) {
    // handle page change
    if (nextProps.match.params.page !== this.props.match.params.page) {
      const data = await getSubforumWithThreads(
        this.props.match.params.id,
        nextProps.match.params.page
      );

      const nsfwFilterEnabled = loadDisplayNsfwFilterSettingFromStorageBoolean();

      if (nsfwFilterEnabled) {
        const filteredThreads = filterNsfwThreads(data.threads);

        return this.setState({
          subforum: { ...data, threads: filteredThreads },
          threadsHidden: true,
        });
      }

      this.setState({ subforum: data });
    }
  }

  render() {
    const {
      subforum: { threads, name, totalThreads },
      threadsHidden,
    } = this.state;
    const {
      match: {
        params: { id },
      },
    } = this.props;
    const currentPage = this.props.match.params.page ? this.props.match.params.page : 1;

    return (
      <SubforumPageComponent
        threads={threads}
        name={name}
        totalThreads={totalThreads}
        currentPage={currentPage}
        id={id}
        threadsHidden={threadsHidden}
      />
    );
  }
}

SubforumPage.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string.isRequired,
      page: PropTypes.string,
    }).isRequired,
  }).isRequired,
};

export default withRouter(SubforumPage);
