import styled from 'styled-components';
import { HashLink as Link } from 'react-router-hash-link';
import { buttonHoverBrightness, MobileMediaQuery } from '../../../components/SharedStyles';
import {
  ThemeVerticalPadding,
  ThemeVerticalHalfPadding,
  ThemeHorizontalPadding,
  ThemeHorizontalHalfPadding,
  ThemeMediumTextSize,
  ThemeSmallTextSize,
  ThemeSecondaryBackgroundColor,
  ThemeTertiaryBackgroundColor,
  ThemeQuaternaryBackgroundColor,
  ThemePrimaryTextColor,
  ThemeSecondaryTextColor,
  ThemeFooterTextColor,
} from '../../../Theme';
import { StyledUserRoleWrapper } from '../../../components/UserRoleWrapper';
import { Avatar } from '../../../components/Avatar';

export const ThreadPageWrapper = styled.article`
  position: relative;

  .threads-hidden-message {
    font-size: 12px;
    opacity: 0.5;
    color: ${ThemeFooterTextColor};
  }
`;

export const ThreadPageBottomControls = styled.div`
  display: block;
  text-align: right;
  padding: 0 ${ThemeHorizontalPadding};
  margin-top: ${ThemeVerticalPadding};
  margin-bottom: 15px;
`;

export const ThreadPageCreateNew = styled(Link)`
  background: #1fa1fd;
  color: white;
  border: none;
  padding: 10px 25px;
  font-size: 15px;
  display: inline-block;
  text-decoration: none;
`;

export const ThreadList = styled.ul`
  margin: 0;
  padding: 0 ${ThemeHorizontalPadding};
`;

export const ThreadPageItem = styled.div`
  position: relative;
  display: flex;
  flex-direction: row;
  width: 100%;
  background: ${ThemeSecondaryBackgroundColor};
  color: white;
  align-items: stretch;
  margin: ${ThemeVerticalHalfPadding} 0;
  box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);
  text-decoration: none;
  border-radius: 5px;
  overflow: hidden;

  ${(props) => props.locked && 'filter: sepia(50%) brightness(80%);'}
  ${(props) => props.deleted && 'filter: grayscale(100%) brightness(80%);'}
  ${(props) => props.hasSeenNoNewPosts && 'filter: opacity(0.5);'}
`;

export const ForumIconWrapper = styled.div`
  position: relative;
  display: flex;
  flex-grow: 0;
  flex-shrink: 0;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  padding: ${ThemeVerticalHalfPadding} ${ThemeHorizontalHalfPadding};
  background: ${ThemeQuaternaryBackgroundColor};
`;

export const ForumIconAlign = styled.div`
  width: 48px;
  height: 48px;

  @media (max-width: 350px) {
    width: 32px;
    height: 32px;
  }
`;

export const ThreadInfoWrapper = styled.div`
  position: relative;
  flex-grow: 1;
  padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};

  .thread-top-rating {
    position: absolute;
    top: 50%;
    right: 0;
    transform: translateY(-50%);
    height: 40%;
    font-size: 13px;
    padding-right: 10px;
    display: flex;
    align-items: center;
    justify-content: right;
    color: ${ThemePrimaryTextColor};

    img {
      height: 100%;
    }

    @media (max-width: 960px) {
      height: 15px;
      bottom: 10px;
      top: unset;
      transform: unset;

      span {
        display: none;
      }
    }

    @media (max-width: 340px) {
      display: none;
    }
  }
`;

export const ThreadTitle = styled(Link)`
  display: flex;
  align-items: center;
  text-decoration: none;
  font-size: ${ThemeMediumTextSize};
  line-height: 1.3;
  word-break: break-word;
  color: ${ThemePrimaryTextColor};

  @media (max-width: 960px) {
    display: inline;
  }
`;

export const ThreadTitlePlaceholder = styled.div`
  background-color: ${ThemeSecondaryTextColor};
  border-radius: 5px;
  width: 135px;
  height: ${ThemeMediumTextSize};
`;

export const ThreadUsernamePlaceholder = styled.div`
  background-color: ${ThemeSecondaryTextColor};
  border-radius: 5px;
  width: 85px;
  height: ${ThemeSmallTextSize};
`;

export const ThreadUserAvatarPlaceholder = styled.div`
  background-color: rgba(0, 0, 0, 0.25);
  width: 40px;
  height: 40px;

  ${MobileMediaQuery} {
    display: none;
  }
`;

export const UnreadPostCount = styled(Link)`
  position: relative;
  background: ${(props) => (props.unreadType === 0 ? `rgb(255, 142, 204)` : `rgb(255, 201, 63)`)};
  padding: 3px 5px;
  border-radius: 3px;
  margin-left: 5px;
  font-size: ${ThemeSmallTextSize};
  text-align: center;
  text-decoration: none;
  display: inline-block;
  height: 21px;
  vertical-align: middle;
  line-height: 21px;
  color: #503714;
  overflow: hidden;

  ${(props) =>
    props.unreadType === 0 &&
    `
  &:after {
    content: '\f004';
    font-family: 'Font Awesome 5 Free';
    position: absolute;
    right: 2px;
    top: 0px;
    font-weight: 600;
    mix-blend-mode: overlay;
    font-size: 18px;
    color: white;
    opacity: 0.4;
  }

  &:before {
    content: '\f004';
    font-family: 'Font Awesome 5 Free';
    position: absolute;
    left: 2px;
    bottom: 0px;
    font-weight: 600;
    mix-blend-mode: overlay;
    font-size: 18px;
    color: white;
    opacity: 0.4;
  }`}

  ${buttonHoverBrightness};
`;

export const ThreadPagination = styled.div`
  margin-top: 3px;
  display: flex;
  align-items: center;

  ${StyledUserRoleWrapper} {
    font-size: 10px;
    margin-right: 10px;
  }

  ${MobileMediaQuery} {
    flex-direction: column;
    align-items: flex-start;

    ${StyledUserRoleWrapper} {
      margin-bottom: 7px;
    }
  }
`;

export const LockedIndicator = styled.i`
  font-size: ${ThemeSmallTextSize};
  color: #e49e2d;
  margin-right: ${ThemeHorizontalHalfPadding};
  line-height: inherit;
`;

export const PinnedIndicator = styled.i`
  font-size: ${ThemeSmallTextSize};
  color: #b4e42d;
  margin-right: ${ThemeHorizontalHalfPadding};
  line-height: inherit;
`;

export const ThreadMetaWrapper = styled(Link)`
  position: relative;
  display: flex;
  flex-grow: 0;
  flex-shrink: 0;
  flex-direction: row;
  color: ${ThemeSecondaryTextColor};
  background-color: ${ThemeTertiaryBackgroundColor};
  width: 300px;
  justify-content: flex-end;
  text-decoration: none;

  ${MobileMediaQuery} {
    padding: 0 ${ThemeHorizontalPadding};
    flex-direction: column;
    justify-content: center;
    width: 100px;
  }
`;

export const ThreadMetaWrapperPlaceholder = styled.div`
  position: relative;
  display: flex;
  flex-grow: 0;
  flex-shrink: 0;
  flex-direction: row;
  color: ${ThemeSecondaryTextColor};
  background-color: ${ThemeTertiaryBackgroundColor};
  width: 300px;
  justify-content: flex-end;
  text-decoration: none;

  ${MobileMediaQuery} {
    padding: 0 ${ThemeHorizontalPadding};
    flex-direction: column;
    justify-content: center;
    width: 100px;
  }
`;

export const ThreadMetaColumn = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};

  &:nth-child(1n) {
    padding: ${ThemeVerticalPadding} 0;
  }
  &:nth-child(2n) {
    padding: ${ThemeVerticalPadding} 0;
  }
  &:nth-child(3n) {
    margin-right: ${ThemeHorizontalPadding};
  }

  span {
    padding: 0;
    margin: ${ThemeVerticalHalfPadding} 0;
    font-size: 12px;
    flex-grow: 1;
    width: 115px;
  }

  span i {
    margin-right: ${ThemeHorizontalHalfPadding};
  }

  span a {
    color: inherit;
    text-decoration: none;
  }

  ${Avatar} {
    width: 40px;
    height: auto;
    margin: auto;
  }

  ${StyledUserRoleWrapper} {
    width: 90px;
    display: block;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    margin: 0;
  }

  ${MobileMediaQuery} {
    padding: 0;

    span {
      margin: ${ThemeVerticalHalfPadding} 0;
      font-size: 10px;
    }

    ${Avatar} {
      display: none;
    }

    &:nth-child(1n) {
      padding: unset;
    }
    &:nth-child(2n) {
      padding: unset;
    }
    &:nth-child(3n) {
      margin-right: unset;
    }
  }
`;
