import { useSelector } from 'react-redux';
import store from '../../state/configureStore';

export const usergroupCheck = (userGroupIds = []) => {
  const state = store.getState();
  if (state && state.user && state.user.usergroup) {
    return userGroupIds.includes(state.user.usergroup);
  }
  return false;
};

const UserGroupRestricted = ({ children, userGroupIds = [] }) => {
  const userState = useSelector((state) => state.user);
  if (userGroupIds.includes(userState.usergroup)) {
    return children;
  }
  return null;
};

export default UserGroupRestricted;
