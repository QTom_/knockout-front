import styled from 'styled-components';
import {
  ThemeBackgroundLighter,
  ThemeFontSizeLarge,
  ThemeFontSizeMedium,
  ThemeFontSizeSmall,
  ThemeHorizontalPadding,
  ThemeTextColor,
  ThemeVerticalPadding,
} from '../../utils/ThemeNew';

export const FieldLabel = styled.div`
  margin-bottom: 10px;
  font-size: ${ThemeFontSizeLarge};
  font-weight: bold;
`;

export const FieldLabelSmall = styled.div`
  margin-bottom: 10px;
  font-size: ${ThemeFontSizeSmall};
  opacity: 0.7;
  line-height: normal;

  a {
    color: #3facff;
  }
`;

export const TextField = styled.input`
  display: block;
  padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
  color: ${ThemeTextColor};
  font-size: ${ThemeFontSizeMedium};
  font-family: 'Open Sans', sans-serif;
  line-height: 1.1;
  margin-bottom: 20px;
  border: none;
  width: 100%;
  box-sizing: border-box;
  background: ${ThemeBackgroundLighter};
`;
