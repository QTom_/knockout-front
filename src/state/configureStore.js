import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger';

import rootReducer from './reducers';

const isDev = process.env.NODE_ENV === 'development';

export function configureStore(initialState = {}) {
  // eslint-disable-next-line no-underscore-dangle
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

  const middlewares = [thunk];
  if (isDev) {
    middlewares.push(createLogger({ collapsed: true }));
  }

  return createStore(rootReducer, initialState, composeEnhancers(applyMiddleware(...middlewares)));
}

const store = configureStore();

export default store;
